package com.Wsdl2Code.WebServices.PassengerService;

import org.kobjects.isodate.IsoDate;
import org.ksoap2.serialization.Marshal;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.util.Date;

/**
 * Created by gomezsso on 23/02/16.
 */
public class MarshalDate implements Marshal {
    @Override
    public Object readInstance(XmlPullParser parser, String namespace, String name, PropertyInfo expected) throws IOException, XmlPullParserException {
        return IsoDate.stringToDate(parser.nextText(), IsoDate.DATE_TIME);
    }

    @Override
    public void writeInstance(XmlSerializer writer, Object instance) throws IOException {
        writer.text(IsoDate.dateToString((Date) instance, IsoDate.DATE_TIME));
    }

    @Override
    public void register(SoapSerializationEnvelope envelope) {
        envelope.addMapping(envelope.xsd, "DateTime", Date.class, this);
    }
}
