package com.Wsdl2Code.WebServices.context;

//------------------------------------------------------------------------------
// <wsdl2code-generated>
//    This code was generated by http://www.wsdl2code.com version  2.5
//
// Date Of Creation: 6/8/2015 12:34:58 AM
//    Please dont change this code, regeneration will override your changes
//</wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code  Version
//
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import java.util.Hashtable;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

public class Passenger implements KvmSerializable {
    
    public int id;
    public String name;
    public String surname;
    public String city;
    public String username;
    
    public Passenger(){}
    
    public Passenger(SoapObject soapObject)
    {
        if (soapObject == null)
            return;
        if (soapObject.hasProperty("id"))
        {
            Object obj = soapObject.getProperty("id");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                id = Integer.parseInt(j.toString());
            }else if (obj!= null && obj instanceof Number){
                id = (Integer) obj;
            }
        }
        if (soapObject.hasProperty("name"))
        {
            Object obj = soapObject.getProperty("name");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                name = j.toString();
            }else if (obj!= null && obj instanceof String){
                name = (String) obj;
            }
        }
        if (soapObject.hasProperty("surname"))
        {
            Object obj = soapObject.getProperty("surname");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                surname = j.toString();
            }else if (obj!= null && obj instanceof String){
                surname = (String) obj;
            }
        }
        if (soapObject.hasProperty("city"))
        {
            Object obj = soapObject.getProperty("city");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                city = j.toString();
            }else if (obj!= null && obj instanceof String){
                city = (String) obj;
            }
        }
        if (soapObject.hasProperty("username"))
        {
            Object obj = soapObject.getProperty("username");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                username = j.toString();
            }else if (obj!= null && obj instanceof String){
                username = (String) obj;
            }
        }
    }
    @Override
    public Object getProperty(int arg0) {
        switch(arg0){
            case 0:
                return id;
            case 1:
                return name;
            case 2:
                return surname;
            case 3:
                return city;
            case 4:
                return username;
        }
        return null;
    }
    
    @Override
    public int getPropertyCount() {
        return 5;
    }
    
    @Override
    public void getPropertyInfo(int index, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info) {
        switch(index){
            case 0:
                info.type = PropertyInfo.INTEGER_CLASS;
                info.name = "id";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "name";
                break;
            case 2:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "surname";
                break;
            case 3:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "city";
                break;
            case 4:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "username";
                break;
        }
    }

    @Override
    public String getInnerText() {
        return null;
    }

    @Override
    public void setInnerText(String s) {

    }

    @Override
    public void setProperty(int arg0, Object arg1) {
    }
    
}
