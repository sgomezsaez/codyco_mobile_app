package com.Wsdl2Code.WebServices.GCMproxy;

//------------------------------------------------------------------------------
// <wsdl2code-generated>
//    This code was generated by http://www.wsdl2code.com version  2.6
//
// Date Of Creation: 7/18/2015 3:25:19 PM
//    Please dont change this code, regeneration will override your changes
//</wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code  Version
//
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import java.util.Hashtable;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

import com.Wsdl2Code.WebServices.PassengerService.*;

public class NotifyInfoType implements KvmSerializable {
    
    public String eventID;
    public VectortripAlternativeType tripAlternatives;
    
    public NotifyInfoType(){}
    
    public NotifyInfoType(SoapObject soapObject)
    {
        if (soapObject == null)
            return;
        if (soapObject.hasProperty("eventID"))
        {
            Object obj = soapObject.getProperty("eventID");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                eventID = j.toString();
            }else if (obj!= null && obj instanceof String){
                eventID = (String) obj;
            }
        }
        if (soapObject.hasProperty("tripAlternatives"))
        {
            SoapObject j = (SoapObject)soapObject.getProperty("tripAlternatives");
            tripAlternatives = new VectortripAlternativeType(j);
        }
    }
    @Override
    public Object getProperty(int arg0) {
        switch(arg0){
            case 0:
                return eventID;
            case 1:
                return tripAlternatives;
        }
        return null;
    }
    
    @Override
    public int getPropertyCount() {
        return 2;
    }
    
    @Override
    public void getPropertyInfo(int index, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info) {
        switch(index){
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "eventID";
                break;
            case 1:
                info.type = PropertyInfo.VECTOR_CLASS;
                info.name = "tripAlternatives";
                break;
        }
    }
    
    @Override
    public String getInnerText() {
        return null;
    }
    
    
    @Override
    public void setInnerText(String s) {
    }
    
    
    @Override
    public void setProperty(int arg0, Object arg1) {
    }
    
}
