package com.Wsdl2Code.WebServices.context_v20;

//------------------------------------------------------------------------------
// <wsdl2code-generated>
//    This code was generated by http://www.wsdl2code.com version  2.6
//
// Date Of Creation: 2/16/2016 2:28:56 PM
//    Please dont change this code, regeneration will override your changes
//</wsdl2code-generated>
//
//------------------------------------------------------------------------------
//
//This source code was auto-generated by Wsdl2Code  Version
//
import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.PropertyInfo;
import java.util.Hashtable;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

public class persistTripAlternativeIDType implements KvmSerializable {
    
    public String passengerID;
    public String requestTimestamp;
    
    public persistTripAlternativeIDType(){}
    
    public persistTripAlternativeIDType(SoapObject soapObject)
    {
        if (soapObject == null)
            return;
        if (soapObject.hasProperty("passengerID"))
        {
            Object obj = soapObject.getProperty("passengerID");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                passengerID = j.toString();
            }else if (obj!= null && obj instanceof String){
                passengerID = (String) obj;
            }
        }
        if (soapObject.hasProperty("requestTimestamp"))
        {
            Object obj = soapObject.getProperty("requestTimestamp");
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                requestTimestamp = j.toString();
            }else if (obj!= null && obj instanceof String){
                requestTimestamp = (String) obj;
            }
        }
    }
    @Override
    public Object getProperty(int arg0) {
        switch(arg0){
            case 0:
                return passengerID;
            case 1:
                return requestTimestamp;
        }
        return null;
    }
    
    @Override
    public int getPropertyCount() {
        return 2;
    }
    
    @Override
    public void getPropertyInfo(int index, @SuppressWarnings("rawtypes") Hashtable arg1, PropertyInfo info) {
        switch(index){
            case 0:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "passengerID";
                break;
            case 1:
                info.type = PropertyInfo.STRING_CLASS;
                info.name = "requestTimestamp";
                break;
        }
    }
    
    @Override
    public String getInnerText() {
        return null;
    }
    
    
    @Override
    public void setInnerText(String s) {
    }
    
    
    @Override
    public void setProperty(int arg0, Object arg1) {
    }
    
}
