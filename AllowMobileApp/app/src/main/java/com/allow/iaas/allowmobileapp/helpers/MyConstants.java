package com.allow.iaas.allowmobileapp.helpers;

/**
 * Created by julian on 6/7/15.
 */
public class MyConstants {

    public static final String IP_ADDR_FRAGMENTO = "129.69.214.228";
    public static final String IP_ADDR_VM = "129.69.214.228";
    public static final String IP_SOAPUI = "141.58.16.16";

    public static final String CONTEXT_HOST = "services2.tsl.gr";

    public static final int PORT_SERVER_FRAGMENTO =8050;
    public static final int PORT_SERVER_VM =8080;
    public static final int PORT_SOAPUI =8088;

    public static final int CONTEXT_HOST_PORT =8080;

    public static String ENDPOINT_ROUTE_MANAGER ="http://"+ IP_ADDR_VM +":" + IP_ADDR_VM + "/IAAS_servicesMockups/services/RouteManager?WSDL";
    public static String ENDPOINT_CONTEXT ="http://"+ CONTEXT_HOST +":" + CONTEXT_HOST_PORT + "/context_v20/services/context_v20/";
    public static String ENDPOINT_ADAPTATION_ENGINE ="http://"+ IP_SOAPUI +":" + PORT_SERVER_VM + "/AdaptationEngineAxis2/services/AdaptationEngineService?wsdl";
    public static String ENDPOINT_GCM ="http://"+ IP_ADDR_VM +":" + PORT_SERVER_VM + "/GoogleCloudMessageProxy/services/GCMproxySOAP?wsdl";
    public static String ENDPOINT_PASSENGER ="http://"+ IP_SOAPUI +":" + PORT_SOAPUI + "/ode/processes/Passenger-PassengerPT-Service/";
    public static boolean IS_PRODUCTION = true;

    public static String PASSENGER_ID_TEST = "c4279e3a-adc8-42474a-a3aea7-6fd9e7c0b52a";

    // Preferences Identifyer
    public static String TRAVEL_TIME = "TravelTime";
    public static String COST = "Cost";
    public static String WALKING_DISTANCE = "WalkingDistance";
    public static String NUMBER_OF_CHANGES = "NoOfChanges";

    // Passenger Type
    public static String PASSENGER_WORKER = "Worker";
    public static String PASSENGER_RETIRED = "Retired";
    public static String PASSENGER_STUDENT = "Student";

    // Decision Policy
    public static String POLICY_UTILITY = "utility";
    public static String POLICY_COST = "price";
    public static String POLICY_DURATION = "duration";
    public static String POLICY_NONE = "none";

}
