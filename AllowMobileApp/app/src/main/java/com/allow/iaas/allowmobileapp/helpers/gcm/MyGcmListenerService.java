package com.allow.iaas.allowmobileapp.helpers.gcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.allow.iaas.allowmobileapp.CustomerActivities.CustomerSelectPaymentActivity;
import com.allow.iaas.allowmobileapp.CustomerActivities.MainCustomerSearchActivity;
import com.allow.iaas.allowmobileapp.CustomerActivities.NotifyEventActivity;
import com.allow.iaas.allowmobileapp.CustomerActivities.TicketQRActivity;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.SelectionActivity;
import com.allow.iaas.allowmobileapp.TaxiActivities.TaxiMapActivity;
import com.google.android.gms.gcm.GcmListenerService;
/**
 * Created by julian on 6/8/15.
 */
public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "GCM";

    @Override
    public void onDeletedMessages() {
        Log.d(TAG, "message deleted");

    }

    @Override
    public void onMessageSent(String msgId) {
        Log.d(TAG, msgId);

    }
    @Override
    public void onSendError(String msgId, String error) {
        Log.d(TAG, msgId + "error: " + error);
    }

    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String msg_type = data.getString("msg_type");
        String message = null;
        String userID =null;
        String tripID =null;
        String transactionID =null;
        String errorMessage =null;
        String paymentType =null;
        String eventID =null;
        String PaymentFragments = null;
        String InitialLatitude = null;
        String InitialLongitude = null;
        String EndLatitude = null;
        String EndLongitude = null;
        Log.d(TAG, "From: " + from);

        switch (msg_type){
            case "notification_msg":
                message = data.getString("message");
                sendNotification(message);
                Log.d(TAG, "Message: " + message);
                break;
            case "request_payment_preference":
                userID = data.getString("user_id");
                PaymentFragments = data.getString("fragments_list");
                selectPaymentPreference(userID, PaymentFragments);
                break;
            case "request_payment_information":
                userID = data.getString("user_id");
                paymentType = data.getString("payment_pref");
                askForPaymentInformation(userID,paymentType );
                break;
            case "send_ticket_information":
                userID = data.getString("user_id");
                tripID = data.getString("trip_id");
                transactionID = data.getString("payment_transaction_id");
                showQRcode(userID, tripID, transactionID);
                break;
            case "send_payment_error":
                userID = data.getString("user_id");
                tripID = data.getString("trip_id");
                errorMessage = data.getString("error_message");
                recievePaymentError(userID,tripID,errorMessage);
                break;
            case "request_ticket":
                userID = data.getString("user_id");
                requestTicket(userID);
                break;
            case "notify_event":
                userID = data.getString("user_id");
                eventID = data.getString("event_id");
                notifyEvent (userID, eventID);
                break;
            case "request_taxi_service":
                userID = data.getString("user_id");
                InitialLatitude = data.getString("init_lat");
                InitialLongitude = data.getString("init_long");
                EndLatitude = data.getString("end_lat");
                EndLongitude = data.getString("end_long");
                requestTaxi (userID, InitialLatitude, InitialLongitude, EndLatitude, EndLongitude);
                break;

        }

    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        Intent intent = new Intent(this, SelectionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_customer)
                .setContentTitle("GCM Message")
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

    private void askForPaymentInformation(String user_id, String payment_pref){

        try{
            Intent intent = new Intent(getApplicationContext(), CustomerSelectPaymentActivity.class);
            intent.putExtra("customer_id",Long.parseLong(user_id));
            intent.putExtra("activity_instruction",CustomerSelectPaymentActivity.INSTRUCTION_SEND_PAYMENT_DETAILS);
            intent.putExtra("payment_pref", payment_pref);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }catch(Exception e){
            Log.e(TAG,"error: ",e);
        }
    }


    private void selectPaymentPreference(String user_id, String fragments_list){

        try{
            Intent intent = new Intent(getApplicationContext(), CustomerSelectPaymentActivity.class);
            intent.putExtra("customer_id",user_id);
            intent.putExtra("fragments_list", fragments_list);
            intent.putExtra("activity_instruction",CustomerSelectPaymentActivity.INSTRUCTION_SELECT_PAYMENT_TYPE);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }catch(Exception e){
            Log.e(TAG,"error: ",e);
        }

    }

    private void showQRcode(String user_id, String trip_id, String transaction_id){
        try{
            Intent intent = new Intent(this, TicketQRActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("ticket_number", Integer.parseInt(trip_id));
            intent.putExtra("ticket_customer_id", Long.parseLong(user_id));
            intent.putExtra("ticket_data", transaction_id);
            intent.putExtra("caller", "payment_confirmation");

            // Getting info from boundle
            intent.putExtra("passengerUUID", MainCustomerSearchActivity.mainSearchActivityBundle.getString("passengerUUID"));
            intent.putExtra("routeID", MainCustomerSearchActivity.mainSearchActivityBundle.getString("routeID"));
            intent.putExtra("passengerName", MainCustomerSearchActivity.mainSearchActivityBundle.getString("passengerName"));
            intent.putExtra("passenger_id", MainCustomerSearchActivity.mainSearchActivityBundle.getLong("pasenger_id"));

            startActivity(intent);
        }catch(Exception e){
            Log.e(TAG,"error: ",e);
        }
    }

    private void recievePaymentError(String user_id, String trip_id, String error_message){
        try{
            Intent intent = new Intent("paymentError");
            intent.putExtra("user_id", user_id);
            intent.putExtra("trip_id", trip_id);
            intent.putExtra("error_message", error_message);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }catch(Exception e){
            Log.e(TAG,"error: ",e);
        }

    }

    private void requestTicket(String PassengerID){
        Intent intent = new Intent(this, TicketQRActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("ticket_customer_id", Long.parseLong(PassengerID));
        intent.putExtra("caller","request_ticket");
        startActivity(intent);

    }

    private void notifyEvent(String PassengerID, String EventID){
        try{
            Intent intent = new Intent(this, NotifyEventActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("user_id", PassengerID);
            intent.putExtra("event_id", EventID);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

            Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_customer)
                    .setContentTitle("GCM Message")
                    .setContentText("press to see alternatives")
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

        }catch(Exception e){
            Log.e(TAG,"error: ",e);
        }


    }

    private void requestTaxi(String Passenger_id, String InitLat, String InitLong, String EndLat, String EndLong){
        Intent intent = new Intent(this, TaxiMapActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("user_id", Passenger_id);
        intent.putExtra("init_lat", InitLat);
        intent.putExtra("init_long", InitLong);
        intent.putExtra("end_lat", EndLat);
        intent.putExtra("end_long", EndLong);
        intent.putExtra("caller","request_taxi_service");
        startActivity(intent);



    }


}
