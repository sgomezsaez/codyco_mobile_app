package com.allow.iaas.allowmobileapp.CustomerActivities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.Wsdl2Code.WebServices.PassengerService.PassengerPassengerPTService;
import com.Wsdl2Code.WebServices.PassengerService.VectorString;
import com.Wsdl2Code.WebServices.PassengerService.VectortripAlternativeType;
import com.Wsdl2Code.WebServices.PassengerService.VectoruserPreference;
import com.Wsdl2Code.WebServices.PassengerService.locationType;
import com.Wsdl2Code.WebServices.PassengerService.passengerTripRequestType;
import com.Wsdl2Code.WebServices.PassengerService.passengerTripResponseType;
import com.Wsdl2Code.WebServices.PassengerService.paymentDataType;
import com.Wsdl2Code.WebServices.PassengerService.routeIDType;
import com.Wsdl2Code.WebServices.PassengerService.tripAlternativeType;
import com.Wsdl2Code.WebServices.PassengerService.userPreference;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.SettingsActivity;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
import com.allow.iaas.allowmobileapp.helpers.RoutesAdapter;
import com.allow.iaas.allowmobileapp.helpers.database.Preference;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;
import com.allow.iaas.allowmobileapp.helpers.fragments.ConfirmPurchaseDialogFragment;
import com.allow.iaas.allowmobileapp.helpers.fragments.selectDateTimeFragment;
import com.allow.iaas.allowmobileapp.helpers.gcm.RegistrationIntentService;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MainCustomerSearchActivity extends FragmentActivity implements LocationListener, selectDateTimeFragment.NoticeDialogListener,
        com.Wsdl2Code.WebServices.PassengerService.IWsdl2CodeEvents, ConfirmPurchaseDialogFragment.PurchaseDialogListener {

    private static final String TAG = "MainSearchActivity" ;
    protected mySQLiteHelper db;
    PassengerPassengerPTService passengerWebservice;
    passengerTripResponseType RequestResponse;
    passengerTripRequestType tripRequest;
    String tmp_token = null;
    //Android elements
    private LocationManager locationManager;
    private Location myLocation;
    private SharedPreferences sharedPref;
    private Geocoder myGeocoder;
    private Calendar mCal;
    //UI elements
    private EditText eT_OriginLocation, eT_DestinyLocation;
    private RadioButton mRadioButtonDeparture, mRadioButtonArrival;
    private TextView tVdepartureTime, tVpreferenceTravelTimeValue, tVPreferenceTravelTimeWeight,
        tVPreferenceCostValue, tVPreferenceCostWeight, tVPreferenceWDValue, tVPreferenceWDWeight,
        tVPreferenceNCValue, tVPreferenceNCWeight, tVPreferenceTMValue;
    private ListView RouteResults;
    private View mProgressView;
    private View mSearchFormView;
    private tripAlternativeType mAlternative = null;
    private boolean isRequestingLocation =false;
    private String pref_modes;
    private String mPrefModes;
    private String policyType;
    private int minute, hour, day, month, year;
    private int max_time;
    private int noofchanges_max;
    private int walkingdistance_max;
    private long passenger_id;
    private String passengerUUID;
    private String passengerName;
    private String selected_time;
    private double cost_max;
    private double time_weight;
    private double cost_weight;
    private double noofchanges_weight;
    private double walkingdistance_weight;
    private BroadcastReceiver mReceiver;
    private ArrayList<Preference> user_preferences;

    public static Bundle mainSearchActivityBundle = new Bundle();

    @Override
    protected void onResume(){
        super.onResume();
        db =  new mySQLiteHelper(getApplicationContext());
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver,new IntentFilter("paymentError"));
        //The values from the shared preferences are read
        sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        readUpDefaultValues();
        if (user_preferences.size() > 0) {

        }
        this.setPreferencesVariables(this.user_preferences);
        tVpreferenceTravelTimeValue.setText(String.valueOf(this.max_time));
        tVPreferenceTravelTimeWeight.setText(Double.toString(this.time_weight));
        tVPreferenceCostValue.setText(Double.toString(this.cost_max));
        tVPreferenceCostWeight.setText(Double.toString(this.cost_weight));
        tVPreferenceWDValue.setText(String.valueOf(this.walkingdistance_max));
        tVPreferenceWDWeight.setText(Double.toString(this.walkingdistance_weight));
        tVPreferenceNCValue.setText(String.valueOf(this.noofchanges_max));
        tVPreferenceNCWeight.setText(Double.toString(this.noofchanges_weight));
        tVPreferenceTMValue.setText(this.pref_modes);
    }

    private void setPreferencesVariables (ArrayList<Preference> preferences) {
        for (Iterator<Preference> i = preferences.iterator(); i.hasNext(); ) {
            Preference p = i.next();
            switch (p.getPreferenceName()) {
                case "TravelTime":
                    this.max_time = (int) Math.round(p.getPreferenceValue());
                    this.time_weight = new BigDecimal(p.getPreferenceWeight()).setScale(4,BigDecimal.ROUND_HALF_EVEN).doubleValue();
                    break;
                case "Cost":
                    this.cost_max = (int) Math.round(p.getPreferenceValue());
                    this.cost_weight = new BigDecimal(p.getPreferenceWeight()).setScale(4,BigDecimal.ROUND_HALF_EVEN).doubleValue();
                    break;
                case "WalkingDistance":
                    this.walkingdistance_max = (int) Math.round(p.getPreferenceValue());
                    this.walkingdistance_weight = new BigDecimal(p.getPreferenceWeight()).setScale(4,BigDecimal.ROUND_HALF_EVEN).doubleValue();;
                    break;
                case "NoOfChanges":
                    this.noofchanges_max = (int) Math.round(p.getPreferenceValue());
                    this.noofchanges_weight = new BigDecimal(p.getPreferenceWeight()).setScale(4,BigDecimal.ROUND_HALF_EVEN).doubleValue();
                    break;

            }
        }
    }

    @Override
    protected void onPause(){
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        db.closeDB();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //TODO: load preferences from context service!!
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_main_search);

        //Retrieve the passenger_id and the preferred transport mean from the previous activity
        passenger_id = getIntent().getLongExtra("passenger_id", 0);
        mPrefModes  = getIntent().getStringExtra("pref_modes");
        user_preferences = (ArrayList<Preference>) getIntent().getSerializableExtra("user_preferences");
        policyType = getIntent().getStringExtra("policyType");
        passengerUUID  = getIntent().getStringExtra("passengerUUID");
        passengerName = getIntent().getStringExtra("passengerName");
        this.setPreferencesVariables(user_preferences);
        //The shared preferences are retrieved
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        //SetUp WebService
        passengerWebservice = new PassengerPassengerPTService(this, MyConstants.ENDPOINT_PASSENGER, 10000);
        //Show Progress Bar
        mProgressView = findViewById(R.id.progressBar_MainSearch);
        mSearchFormView = findViewById(R.id.LinearLayout_mainSearch);

        eT_OriginLocation = (EditText) findViewById(R.id.editText_origin_location);
        eT_DestinyLocation = (EditText) findViewById(R.id.editText_destiny_location);
        tVdepartureTime = (TextView) findViewById(R.id.textView_departureTime);
        tVpreferenceTravelTimeValue = (TextView) findViewById(R.id.tVpreferenceTravelTimeValue);
        tVPreferenceTravelTimeWeight = (TextView) findViewById(R.id.tVPreferenceTravelTimeWeight);
        tVPreferenceCostValue = (TextView) findViewById(R.id.tVPreferenceCostValue);
        tVPreferenceCostWeight = (TextView) findViewById(R.id.tVPreferenceCostWeight);
        tVPreferenceNCValue = (TextView) findViewById(R.id.tVPreferenceNCValue);
        tVPreferenceNCWeight = (TextView) findViewById(R.id.tVPreferenceNCWeight);
        tVPreferenceWDValue = (TextView) findViewById(R.id.tVPreferenceWDValue);
        tVPreferenceWDWeight = (TextView) findViewById(R.id.tVPreferenceWDWeight);
        tVPreferenceTMValue = (TextView) findViewById(R.id.tVPreferenceTMValue);
        mRadioButtonDeparture = (RadioButton) findViewById(R.id.radioButton_departure);
        mRadioButtonArrival= (RadioButton) findViewById(R.id.radioButton_arrival);
        RouteResults = (ListView) findViewById(R.id.listView_ResultRoutes);

        RouteResults.setOnTouchListener(new View.OnTouchListener() {
            // Setting on Touch Listener for handling the touch inside ScrollView
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                // Disallow the touch request for parent scroll on touch of child view
                v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        myGeocoder = new Geocoder(this);
        setUpControlWithCurrentTime();

        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String user_id = intent.getStringExtra("user_id");
                String trip_id = intent.getStringExtra("trip_id");
                String error_msg =intent.getStringExtra("error_message");
                Toast.makeText(getApplicationContext(),"error purchasing tickets", Toast.LENGTH_SHORT).show();
            }
        };
    }

    /**
     * Setup of the UI controller to show the current date and time
     */
    private void setUpControlWithCurrentTime(){
        mCal = Calendar.getInstance();
        minute = mCal.get(Calendar.MINUTE);
        hour  = mCal.get(Calendar.HOUR);
        day  = mCal.get(Calendar.DAY_OF_MONTH);
        month  = mCal.get(Calendar.MONTH);
        year  = mCal.get(Calendar.YEAR);
        tVdepartureTime.setText("Departing on " + day + "." + month + "." + year + ", at " + hour + ":" + minute);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_customer_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent myIntent = new Intent(this, SettingsActivity.class);
                startActivity(myIntent);
                break;
            case R.id.action_about:
                showAboutDialog();
            default:
                break;
        }
        return true;
    }

    /**
     * Handles click on Request button
     * @param view
     */
    public void onRequestClick(View view){
        double originLatitude = 0, originLongitude =0, destinyLatitude =0, destinyLongitude =0;
        int originCluster = 0, destinationCluster = 0;

        if (isNetworkAvailable()){
            if (!(eT_OriginLocation.getText().toString()).equals("") && !(eT_DestinyLocation.getText().toString()).equals("")){
                int RequestResponse=-1;
                if (eT_OriginLocation.getText().toString().matches("\\d+(?:\\.\\d+)?") &&
                        eT_DestinyLocation.getText().toString().matches("\\d+(?:\\.\\d+)?")) {
                    Log.i(TAG, "Using Clusters for the Location");
                    originCluster = Integer.parseInt(eT_OriginLocation.getText().toString());
                    destinationCluster = Integer.parseInt(eT_DestinyLocation.getText().toString());
                    originLatitude = -1;
                    originLongitude = -1;
                    destinyLatitude = -1;
                    destinyLongitude = -1;
                }
                else {
                    List<Address> addressesOriginList;
                    List<Address> addressesDestinyList;
                    try {
                        addressesOriginList = myGeocoder.getFromLocationName(eT_OriginLocation.getText().toString(),1);
                        addressesDestinyList = myGeocoder.getFromLocationName(eT_DestinyLocation.getText().toString(),1);
                        if(addressesOriginList.size() > 0) {
                            Address address_origin = addressesOriginList.get(0);
                            originLatitude =address_origin.getLatitude();
                            originLongitude = address_origin.getLongitude();
                        }
                        if(addressesDestinyList.size() > 0) {
                            Address address_destination = addressesDestinyList.get(0);
                            destinyLatitude = address_destination.getLatitude();
                            destinyLongitude = address_destination.getLongitude();
                        }
                        originCluster = -1;
                        originCluster = -1;
                    } catch (IOException e) {
                        Log.e(TAG, "Error Geocoding the Locations");
                        return;
                    }
                }

                //At this Point the locations are Geodecoded
                if (originLatitude != 0 && originLongitude != 0 && destinyLatitude !=0
                        && destinyLongitude !=0 && originCluster != 0 && destinationCluster != 0){
                    try {
                        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                        tmp_token = sharedPreferences.getString(RegistrationIntentService.REG_ID, "");

                        passengerTripRequestType requestData = null;
                        //Depending if it departure or arrival time the corresponding request is sent
                        if(mRadioButtonDeparture.isChecked()) {
                           requestData = createRequest(tmp_token,originLatitude,
                                    originLongitude,destinyLatitude,destinyLongitude,originCluster,
                                    destinationCluster,selected_time,true, policyType);

                        }else if (mRadioButtonArrival.isChecked()) {
                            requestData = createRequest(tmp_token,originLatitude,
                                    originLongitude,destinyLatitude,destinyLongitude,
                                    originCluster, destinationCluster,
                                    selected_time,false, policyType);
                        }
                        if (requestData != null) {
                            passengerWebservice.setUrl(MyConstants.ENDPOINT_PASSENGER);
                            passengerWebservice.setTimeOut(120);
                            passengerWebservice.passengerTripRequestAsync(requestData);
                        }
                        Toast.makeText(this, "Performing Trip Request", Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Log.e(TAG, "Error with CreateRequest");
                        Toast.makeText(this, "Error with request", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }else{
            Toast.makeText(this, "Network not available, please connect to network", Toast.LENGTH_LONG).show();
        }
    }


    private passengerTripRequestType createRequest(String Token,
                                                   double origin_latitude,double origin_longitude,
                                                   double destiny_latitude, double destiny_longitude,
                                                   int originCluster, int destinationCluster,
                                                   String selected_time, boolean isDeparture,
                                                   String decisionPolicy){

        passengerTripRequestType request = new passengerTripRequestType();
        request.userFrontendToken = Token;
        userPreference prefe_max_time = new userPreference();
        prefe_max_time.preferenceID=MyConstants.TRAVEL_TIME;
        prefe_max_time.preferenceValue=String.valueOf(max_time);
        prefe_max_time.preferenceWeight=time_weight;
        userPreference prefe_cost_max = new userPreference();
        prefe_cost_max.preferenceID=MyConstants.COST;
        prefe_cost_max.preferenceValue=String.valueOf(cost_max);
        prefe_cost_max.preferenceWeight=cost_weight;
        userPreference prefe_noofchanges = new userPreference();
        prefe_noofchanges.preferenceID=MyConstants.NUMBER_OF_CHANGES;
        prefe_noofchanges.preferenceValue=String.valueOf(noofchanges_max);
        prefe_noofchanges.preferenceWeight=noofchanges_weight;
        userPreference prefe_walkingdistance_max = new userPreference();
        prefe_walkingdistance_max.preferenceID=MyConstants.WALKING_DISTANCE;
        prefe_walkingdistance_max.preferenceValue=String.valueOf(walkingdistance_max);
        prefe_walkingdistance_max.preferenceWeight= walkingdistance_weight;
        VectoruserPreference vectorUserPrefs = new VectoruserPreference();
        vectorUserPrefs.add(prefe_max_time);
        vectorUserPrefs.add(prefe_cost_max);
        vectorUserPrefs.add(prefe_noofchanges);
        vectorUserPrefs.add(prefe_walkingdistance_max);
        request.userPreferences = vectorUserPrefs;

        request.passengerID = passengerUUID;
        request.name = passengerName;

        if (isDeparture){
            request.departTimeSpecified = true;
            request.departTime = selected_time;
            request.arrivalTimeSpecified =false;
            request.arrivalTime = "00:00:00";
        }else{
            request.arrivalTimeSpecified =true;
            request.arrivalTime = selected_time;
            request.departTimeSpecified = false;
            request.departTime = "00:00:00";
        }


        locationType startLocation = new locationType();
        locationType endLocation = new locationType();
        if ((originCluster != 0) && (destinationCluster != -1)) {
            request.startClusterRegion = originCluster;
            request.startClusterRegionSpecified = true;
            request.endClusterRegion = destinationCluster;
            request.endClusterRegionSpecified = true;
            startLocation.latitude = 0.0;
            startLocation.longitude = 0.0;
            endLocation.latitude = 0.0;
            endLocation.longitude = 0.0;
        }
        else {
            startLocation.latitude = origin_latitude;
            startLocation.longitude = origin_longitude;
            endLocation.latitude = destiny_latitude;
            endLocation.longitude = destiny_longitude;
            request.startClusterRegion = -1;
            request.startClusterRegionSpecified = false;
            request.endClusterRegion = -1;
            request.endClusterRegionSpecified = false;
        }
        request.start_loc = startLocation;
        request.end_loc = endLocation;

        VectorString transportationModesList = new VectorString();
        transportationModesList.add(String.valueOf(pref_modes));
        request.transportationModes = transportationModesList;
        request.policyType = decisionPolicy;

        return request;
    }

    /**
     * Checks if the cell phone is connected and available
     * @return true if the phone is connected to the network, if not returns false
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * The button to SetUp date and Time is pressed
     * @param view
     */
    public void setTimeDate(View view){
        DialogFragment newFragment = new selectDateTimeFragment();
        newFragment.show(getFragmentManager(), "Select Time");
    }

    /**
     * The button get current Position is pressed, it toggles the activation between of the GPS or not
     * the GPS is also automatically turned off if a location with less than 30 of precision if obtained
     * @param view
     */
    public void getCurrentPosition(View view){
        if(!isRequestingLocation){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
            isRequestingLocation=true;
        }else{
            locationManager.removeUpdates(this);
            isRequestingLocation=false;
        }
    }

    /**
     * The Name of the destination and the name of the destiny are switched
     * @param view
     */
    public void switchOriginAndDestiny(View view){
        if (!(eT_OriginLocation.getText().toString()).equals("") || !(eT_DestinyLocation.getText().toString()).equals("")){
            String tempOrigin = eT_OriginLocation.getText().toString();
            String tempDestiny = eT_DestinyLocation.getText().toString();
            eT_OriginLocation.setText(tempDestiny);
            eT_DestinyLocation.setText(tempOrigin);
        }else{
            Toast t  = Toast.makeText(this,"An Origin Location and a destination are required",Toast.LENGTH_SHORT);
            t.show();
        }
    }

    public void showMyTickets(View view){
        Intent intent = new Intent(this,TicketsListActivity.class);
        intent.putExtra("customer_id", passenger_id);
        intent.putExtra("passengerUUID", passengerUUID);
        intent.putExtra("passengerName", passengerName);
        startActivity(intent);
    }

    @Override
    public void onLocationChanged(Location location) {
        myLocation =location;
        if (myLocation.getAccuracy()<30){
            try {
                List<Address> addresses = myGeocoder.getFromLocation(myLocation.getLatitude(), myLocation.getLongitude(),1);
                if(addresses.size() > 0) {
                    Address myAdd = addresses.get(0);
                    String street = myAdd.getThoroughfare();
                    eT_OriginLocation.setText(street);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            locationManager.removeUpdates(this);
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    //****** Implementation of NoticeDialogListener ******
    //Press Set Time on the Fragment Dialog
    @Override
    public void onDialogPositiveClick(int hour, int minute, int day, int month, int year) {
        this.hour =hour;
        this.minute = minute;
        this.day = day;
        this.month =month;
        this.year = year;
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hour,minute);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
        selected_time = sdf.format(calendar.getTime());
        tVdepartureTime.setText("Departing on " + day + "." + month + "." + year + ", at " + hour + ":" + minute);
    }
    //User closed the Set Date and Time Dialog
    @Override
    public void onDialogNegativeClick() {

    }
    //****************************************************



    public void readUpDefaultValues(){
        if (mPrefModes.equals("")){
            pref_modes = sharedPref.getString(getString(R.string.key_preference_pref_modes), "bus");
        }else{
            pref_modes = mPrefModes;
        }
        max_time = sharedPref.getInt(getString(R.string.key_preference_time_max), 30);
        cost_max = sharedPref.getInt(getString(R.string.key_preference_cost_max), 15);
        noofchanges_max = Integer.parseInt(sharedPref.getString(getString(R.string.key_preference_pref_noofchanges_max), "1"));
        walkingdistance_max = sharedPref.getInt(getString(R.string.key_preference_pref_walkingdistance_max), 100);
        time_weight =Double.parseDouble(sharedPref.getString(getString(R.string.key_pref_time_weight), "0.5"));
        cost_weight =Double.parseDouble(sharedPref.getString(getString(R.string.key_pref_cost_weight), "0.5"));
        noofchanges_weight =Double.parseDouble(sharedPref.getString(getString(R.string.key_pref_noofchanges_weight), "0.5"));
        walkingdistance_weight =Double.parseDouble(sharedPref.getString(getString(R.string.key_pref_walkingdistance_weight), "0.5"));
    }

    /**
     * Created to show the About Information
     */
    public void showAboutDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        // Setting Dialog Title
        alertDialog.setTitle("About Allow Ensembles");
        // Setting Dialog Message
        alertDialog.setMessage("IAAS Uni Stuttgart \n Ver 0.1a \n Contact: julian.martinez.salazar@gmail.com");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.ic_launcher);
        // Setting OK Button
        alertDialog.setButton("Accept", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                Toast.makeText(getApplicationContext(), "IAAS 2015", Toast.LENGTH_SHORT).show();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void Wsdl2CodeStartedRequest() {
        showProgress(true);
    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object Data) {

        switch(methodName){

            case "passengerTripRequest":
                try{
                    RequestResponse = (passengerTripResponseType)Data;

                    int numberOfRoutes = RequestResponse.tripAlternatives.size();
                    System.out.println("Number of Trip Alternatives Received: " + numberOfRoutes);

                    List<tripAlternativeType> alternatives = RoutesAdapter.VectorToList(RequestResponse.tripAlternatives);
                    alternatives = RoutesAdapter.orderListByUtility(alternatives);



                    RoutesAdapter mAlternativesAdapter = new RoutesAdapter(this, alternatives);
                    RouteResults.setAdapter(mAlternativesAdapter);


                    RouteResults.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            tripAlternativeType myTripAlternative = (tripAlternativeType) parent.getItemAtPosition(position);
                            //Map.Entry<String, String> selectedValue = ( Map.Entry<String, String>)parent.getItemAtPosition(position);

                            Toast.makeText(getApplicationContext(), myTripAlternative.route.getRouteSummary(), Toast.LENGTH_SHORT).show();
                            Bundle mArgs = new Bundle();
                            mArgs.putInt("position", position);
                            mArgs.putString("routeID", myTripAlternative.routeID);

                            DialogFragment purchaseFragment = new ConfirmPurchaseDialogFragment();
                            purchaseFragment.setArguments(mArgs);
                            purchaseFragment.show(getFragmentManager(), "Purchase Ticket");



                        }
                    });

                    Toast.makeText(this, "Confirming Trip Alternative Selection", Toast.LENGTH_SHORT).show();

                }catch (Exception e){
                    Log.e(TAG,"error en adapter",e);
                }
                break;
            case "receiveTripAlternatives":
                //Toast.makeText(this, "Route Confirmed. Starting Buying Process", Toast.LENGTH_LONG).show();
                System.out.println("Route Confirmed. Starting Buying Process");

                if (tmp_token == null) {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
                    tmp_token = sharedPreferences.getString(RegistrationIntentService.REG_ID, "");
                }

                System.out.println("ser Token: " + tmp_token);

                //TODO: get credit card. If the passenger has credit card, then send the payment data. If not, then send it empty
                paymentDataType paymentData = new paymentDataType();
                paymentData.paymentAmount = mAlternative.route.routeTotalCost;

                String[] passengerCreditCard = db.getCustomersCreditCardsList(passenger_id);
                if (passengerCreditCard != null && passengerCreditCard.length > 0) {
                    paymentData.paymentMethod = passengerCreditCard[0];
                    paymentData.paymentMethodName = passengerName;
                    long crediCardDBKey = db.getPrimaryKeyForCustomerCreditCard(paymentData.paymentMethod, passenger_id);

                    Map<String,String> creditCardDetails = db.getCreditCardDetails(crediCardDBKey);
                    paymentData.paymentMethodNumber = creditCardDetails.get(db.COLUMN_CARD_CARDNUMBER);
                    System.out.println("Credit Card Details retrieved...");

                }
                else {
                    paymentData.paymentMethodName = "tbd";
                    paymentData.paymentMethod = "tbd";
                    paymentData.paymentMethodNumber = "tbd";
                }

                routeIDType routeID = new routeIDType();
                routeID.startCluster = mAlternative.route.routeID.startCluster;
                routeID.endCluster = mAlternative.route.routeID.endCluster;
                routeID.routeID = mAlternative.routeID;
                paymentData.routeID = routeID;

                try {
                    passengerWebservice.requestPaymentAsync(passengerUUID, "tripBooking", tmp_token, paymentData);

                } catch (Exception e) {
                    Toast.makeText(this, "Payment could not be requested", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
                break;
            case "requestPayment":
                System.out.println("Request Payment Finished. Now Waiting for GCM...");
                System.out.println("Request Payment Finished. Now Waiting for GCM...");
                Toast.makeText(this, "Waiting for Payment...", Toast.LENGTH_LONG).show();

                mainSearchActivityBundle.putString("passengerUUID", this.passengerUUID);
                mainSearchActivityBundle.putString("routeID", mAlternative.routeID);
                mainSearchActivityBundle.putString("passengerName", this.passengerName);
                mainSearchActivityBundle.putLong("pasenger_id", this.passenger_id);

                break;
        }
    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {

    }

    @Override
    public void Wsdl2CodeEndedRequest() {
        showProgress(false);
    }

    /**
     * Shows the progress UI and hides the login form, during the ProgressBar Animation
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            // if (false) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mSearchFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSearchFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSearchFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSearchFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     *     This click is from the DialogFragment when Buy is pressed, it provides the index of the selected Item
     */
    @Override
    public void onBuyClick(int position, String routeID) {

        boolean existTripAlternative =false;
        tripAlternativeType mAlternative = getTripAlternative(routeID, RequestResponse.tripAlternatives);
        this.mAlternative = mAlternative;
        if (mAlternative != null){
            existTripAlternative =true;
            System.out.println("Trip alternative Exists");
        }
        try {
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            passengerWebservice.receiveTripAlternativesAsync(passengerUUID, existTripAlternative, timeStamp, mAlternative);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private tripAlternativeType getTripAlternative (String routeID, VectortripAlternativeType v) {
        for (Iterator<tripAlternativeType> i = v.iterator(); i.hasNext() ;) {
            tripAlternativeType tripAlternative = i.next();
            if (tripAlternative.routeID.equals(routeID)) {
                return tripAlternative;
            }
        }
        return null;
    }

    /**
     * If the back button is pressed, the it must be informed that no alternative was selected
     */
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        try {
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());
            passengerWebservice.receiveTripAlternativesAsync(passengerUUID, false, timeStamp, null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Sends the transaction State to the Execution Engine
     * @param status
     * @param transactionID
     */
    public void sendTransactionState(boolean status, String transactionID){

        try {
            if (status){
                //TODO: fix!!
                //passengerWebservice.receiveTransactionStatus(String.valueOf(passenger_id),"ok",transactionID);
            }else{
                //TODO: fix!!
                //passengerWebservice.receiveTransactionStatus(String.valueOf(passenger_id),"error",transactionID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
