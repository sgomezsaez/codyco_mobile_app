package com.allow.iaas.allowmobileapp.BusActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.Wsdl2Code.WebServices.RouteManager.RouteManager;
import com.Wsdl2Code.WebServices.context.IWsdl2CodeEvents;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.database.Ticket;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.HashMap;
import java.util.Map;


public class BusPassengerCheckInActivity extends Activity implements IWsdl2CodeEvents{

    protected mySQLiteHelper db;
    private Map<Long,Ticket> purchasedTickets = new HashMap<>();
    private ListView checkedTickets;
    private ArrayAdapter myAdapter;

    @Override
    protected void onResume() {
        super.onResume();
        db =  new mySQLiteHelper(getApplicationContext());
        purchasedTickets = db.getPurchasedTicketsList();
        if(purchasedTickets.size() >0){

            String[] arr = new String[purchasedTickets.size()];
            int i=0;
            for(Map.Entry<Long,Ticket> mEntry: purchasedTickets.entrySet()){
                arr[i]= String.valueOf(mEntry.getValue().getTicketNr());
                i++;
            }
            myAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,arr);
            checkedTickets.setAdapter(myAdapter);
            checkedTickets.setOnItemClickListener( new customListener());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.closeDB();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_passenger_check_in);
        checkedTickets = (ListView) findViewById(R.id.listViewCheckedPassengers);
        db =  new mySQLiteHelper(getApplicationContext());
        purchasedTickets = db.getPurchasedTicketsList();
        int elem = purchasedTickets.size();
        /**
         if(elem >0){
         String[] arr = (String[]) purchasedTickets.values().toArray();
         myAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,arr);
         checkedTickets.setAdapter(myAdapter);
         checkedTickets.setOnItemClickListener( new customListener());
         }
         **/

    }



    private class customListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_bus__passenger_check_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onScanTicketClick(View view) {
        IntentIntegrator integrator = new IntentIntegrator(this);
        integrator.initiateScan();
    }


    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
        if (scanResult != null) {
            // handle scan result
            String result = scanResult.getContents();
            String[]data = result.split(":");
            int customer_id = Integer.parseInt(data[0]);
            int ticketNr =  Integer.parseInt(data[1]);
//TODO:
            /**
             * Store  here the information in the  db.createPurchasedTicket();
             */


            RouteManager routeManagerService = new RouteManager(this);
            routeManagerService.setUrl(MyConstants.ENDPOINT_ROUTE_MANAGER);
            try {
                routeManagerService.SubmitCheckInPassengerAsync(customer_id, ticketNr);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void Wsdl2CodeStartedRequest() {

    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object Data) {

        switch(methodName){
            case "SubmitCheckInPassenger":
                boolean response = (boolean)Data;
                if (response){
                    Toast.makeText(getApplicationContext(),"It is a valid Ticket",Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(getApplicationContext(),"It is not a valid Ticket",Toast.LENGTH_LONG).show();
                }
                break;
            case "m2":
                break;
        }

    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {

    }

    @Override
    public void Wsdl2CodeEndedRequest() {

    }

}
