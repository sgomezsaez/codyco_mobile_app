package com.allow.iaas.allowmobileapp.CustomerActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.Wsdl2Code.WebServices.GCMproxy.GCMproxy;
import com.Wsdl2Code.WebServices.GCMproxy.PaymentInfo;
import com.Wsdl2Code.WebServices.context.IWsdl2CodeEvents;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public class CustomerSelectPaymentActivity extends Activity implements IWsdl2CodeEvents{

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String SENDER_ID = "1093769792435";
    private static final String TAG = "CustomerSelPayment";

    public static final int INSTRUCTION_SELECT_PAYMENT_TYPE = 1;
    public static final int INSTRUCTION_SEND_PAYMENT_DETAILS = 2;

    //Helpers
    private GoogleCloudMessaging gcm;
    protected mySQLiteHelper db;
    Gson gson;
    List<String> paymentFragments;

    //UI elements
    private Button Button_pay;
    private Spinner SpinnerPayment, SpinnerCard;
    //Intent Extras
    String customer_id;
    int activity_instruction;
    String payment_pref;
    //Web Service
    private GCMproxy gcmService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        customer_id = intent.getStringExtra("customer_id");
        activity_instruction = intent.getIntExtra("activity_instruction", 0);
        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
        gson = new Gson();
        setContentView(R.layout.activity_customer_select_payment);
        SpinnerPayment = (Spinner) findViewById(R.id.spinner_PaymentMethod);
        SpinnerCard = (Spinner) findViewById(R.id.spinner_pickupCard);
        Button_pay = (Button) findViewById(R.id.button_pay);
        gcmService  = new GCMproxy(this);
        gcmService.setUrl(MyConstants.ENDPOINT_GCM);

        ArrayAdapter mAdapPayment = null;
        //If the Activity was called because it was requested to send the payment method
        if (activity_instruction == INSTRUCTION_SELECT_PAYMENT_TYPE){
            String frag_list = intent.getStringExtra("fragments_list");
            Button_pay.setText("Confirm Payment Method");
            paymentFragments = gson.fromJson(frag_list,List.class);
            String[] mFrag = paymentFragments.toArray(new String[0]);
            mAdapPayment = new ArrayAdapter(this,android.R.layout.simple_list_item_1,mFrag);
            SpinnerCard.setVisibility(View.GONE);
        }else if (activity_instruction == INSTRUCTION_SEND_PAYMENT_DETAILS){
            payment_pref = intent.getStringExtra("payment_pref");
            Button_pay.setText("Pay");
            String[]paymentTypes = {"Select Card Type","Visa", "Mastercard", "AmericanExpress", "Add Card"};
            mAdapPayment = new ArrayAdapter(this,android.R.layout.simple_list_item_1,paymentTypes);
            SpinnerCard.setVisibility(View.VISIBLE);
        }
        SpinnerPayment.setAdapter(mAdapPayment);
    }

    @Override
    protected void onResume(){
        super.onResume();
        db  = new mySQLiteHelper(getApplicationContext());
        if (activity_instruction == INSTRUCTION_SEND_PAYMENT_DETAILS){
            SpinnerPayment.setOnItemSelectedListener(new customOnItemSelectedListener());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.closeDB();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer_select_payment, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void Wsdl2CodeStartedRequest() {

    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object Data) {
        switch(methodName){
            case "SendPaymentPreference":
                System.out.println("Payment Preference Sent, waiting for ticket...");
                Toast.makeText(this, "Waiting for Ticket...", Toast.LENGTH_LONG).show();

                break;
            case "SendPaymentInfo":
                System.out.println("Payment Info Sent, waiting for ticket...");
                Toast.makeText(this, "Waiting for Ticket...", Toast.LENGTH_LONG).show();
                break;
        }
        finish();

    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {

    }

    @Override
    public void Wsdl2CodeEndedRequest() {
        finish();
    }


    private class customOnItemSelectedListener implements AdapterView.OnItemSelectedListener{


        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String Card = (String) parent.getItemAtPosition(position);
            String[] CardNumbers = null;
            Map<Long,String> cards = null;
            ArrayAdapter  mCardAdapter =null;

            if (Card.toLowerCase().equals("visa") || Card.toLowerCase().equals("v")) {
                cards = db.getCardsByCustomerIDandType(customer_id, "Visa");
                if (cards.size()<=0){
                    CardNumbers = new String[]{"No Card"};
                }else{
                    CardNumbers = cards.values().toArray(new String[0]);
                }
                mCardAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,CardNumbers);
                SpinnerCard.setAdapter(mCardAdapter);
            }
            else if (Card.toLowerCase().equals("mastercard") || Card.toLowerCase().equals("mc")) {
                cards = db.getCardsByCustomerIDandType(customer_id, "Mastercard");
                if (cards.size()<=0){
                    CardNumbers = new String[]{"No Card"};
                }else{
                    CardNumbers = cards.values().toArray(new String[0]);
                }
                mCardAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,CardNumbers);
                SpinnerCard.setAdapter(mCardAdapter);
            }
            else if (Card.toLowerCase().equals("americanexpress") || Card.toLowerCase().equals("ae")) {
                cards = db.getCardsByCustomerIDandType(customer_id, "AmericanExpress");
                if (cards.size()<=0){
                    CardNumbers = new String[]{"No Card"};
                }else{
                    CardNumbers = cards.values().toArray(new String[0]);
                }
                mCardAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,CardNumbers);
                SpinnerCard.setAdapter(mCardAdapter);
            }
            else if (Card.toLowerCase().equals("Add Card")) {
                Intent mIntent = new Intent(getApplicationContext(), CardDetailsActivity.class);
                mIntent.putExtra("operation", CardDetailsActivity.CARD_CREATE);
                mIntent.putExtra("user-type",CustomerSignUpActivity.EXISTENT_USER);
                mIntent.putExtra("customer_id", customer_id);
                startActivity(mIntent);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }


    private void populatePayMethods(String payment_pref){
        String[] CardNumbers = null;
        Map<Long,String> cards = db.getCardsByCustomerIDandType(customer_id, payment_pref);
        if (cards.size()<=0){
            CardNumbers = new String[]{"No Card"};
        }else{
            CardNumbers = cards.values().toArray(new String[0]);
        }
        ArrayAdapter mCardAdapter = new ArrayAdapter(getApplicationContext(),android.R.layout.simple_list_item_1,CardNumbers);
        SpinnerCard.setAdapter(mCardAdapter);
    }


    public void onBuyClick(View view){

        if (activity_instruction == INSTRUCTION_SEND_PAYMENT_DETAILS){

            String CardAlias = SpinnerCard.getSelectedItem().toString();
            if(CardAlias.length()>0 && !CardAlias.equals("No Card")){
                Long CardKey = db.getPrimaryKeyForCustomerCreditCard(CardAlias, customer_id);
                if(CardKey>0){
                    PaymentInfo payInfo = getPaymentInformation(CardKey);
                    gcmService.SendPaymentInfo(payInfo);
                }else{
                    Toast.makeText(getApplicationContext(),"No Card in DB", Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(getApplicationContext(),"No Card selected", Toast.LENGTH_SHORT).show();
            }
        }else{
            String CardPref = SpinnerPayment.getSelectedItem().toString();
            try {
                gcmService.SendPaymentPreferenceAsync(CardPref);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private PaymentInfo getPaymentInformation(long CardKey){
        PaymentInfo payInfo = null;
        Map<String, String> cardInfo = db.getCreditCardDetails(CardKey);
        if (cardInfo.size()>0 ){
            payInfo = new PaymentInfo();
            for(Map.Entry<String,String> mEntry: cardInfo.entrySet()){
                switch(mEntry.getKey()){
                    case mySQLiteHelper.COLUMN_CARD_CARDHOLDER_NAME:
                        payInfo.cardholder = mEntry.getValue();
                        break;
                    case mySQLiteHelper.COLUMN_CARD_CARDNUMBER:
                        payInfo.number = mEntry.getValue();
                        break;
                    case mySQLiteHelper.COLUMN_CARD_CVV:
                        payInfo.cvv = mEntry.getValue();
                        break;
                }
            }
        }
        return payInfo;
    }

}
