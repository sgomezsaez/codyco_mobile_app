package com.allow.iaas.allowmobileapp.helpers.database;

import java.io.Serializable;

/**
 * Created by sgomezsaez on 2/16/2016.
 */
public class Preference implements Serializable{

    private String preferenceID;
    private String preferenceName;
    private Double preferenceValue;
    private Double preferenceWeight;

    public Preference (){}
    public Preference (String preferenceID, String preferenceName, Double preferenceValue, Double preferenceWeight){
        this.preferenceID = preferenceID;
        this.preferenceName = preferenceName;
        this.preferenceValue = preferenceValue;
        this.preferenceWeight = preferenceWeight;
    }

    public void setPreferenceID (String preferenceID) {
        this.preferenceID = preferenceID;
    }

    public void setPreferenceName (String preferenceName) {
        this.preferenceName = preferenceName;
    }

    public void setPreferenceValue (Double preferenceValue) {
        this.preferenceValue = preferenceValue;
    }

    public void setPreferenceWeight (Double preferenceWeight) {
        this.preferenceWeight = preferenceWeight;
    }

    public String getPreferenceID () {
        return this.preferenceID;
    }

    public String getPreferenceName () {
        return this.preferenceName;
    }

    public Double getPreferenceValue () {
        return this.preferenceValue;
    }

    public Double getPreferenceWeight () {
        return this.preferenceWeight;
    }
}
