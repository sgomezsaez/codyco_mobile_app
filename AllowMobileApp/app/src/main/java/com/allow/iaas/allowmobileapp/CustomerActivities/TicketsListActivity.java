package com.allow.iaas.allowmobileapp.CustomerActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.database.Ticket;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;

import java.util.Map;


public class TicketsListActivity extends Activity {

    ListView ticketList;
    protected mySQLiteHelper db;
    private  Map<Long,Ticket> tickets;
    long customer_id;
    String customerUUID;
    String customerName;


    @Override
    protected void onPause() {
        super.onPause();
        db.closeDB();
    }

    @Override
    protected void onResume(){
        super.onResume();
        db  = new mySQLiteHelper(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_tickets_list);

        customer_id = getIntent().getLongExtra("customer_id", 0);
        customerUUID = getIntent().getStringExtra("passengerUUID");
        customerName = getIntent().getStringExtra("passengerName");
        ticketList = (ListView) findViewById(R.id.listView_ticketList);
        db  = new mySQLiteHelper(getApplicationContext());
        if (customer_id > 0){
            tickets = db.getPurchasedTicketsList(customer_id);
        }

        String[] ticketsNrs;
        if (tickets.size()>0){
            ticketsNrs = new String[tickets.size()];
            int i=0;
            for(Map.Entry<Long,Ticket> mEntry: tickets.entrySet()){
                ticketsNrs[i]= String.valueOf(mEntry.getValue().getTicketNr());
                i++;
            }
            ArrayAdapter mAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,ticketsNrs);
            ticketList.setAdapter(mAdapter);
            ticketList.setOnItemClickListener(new mCustomOnClickListener());
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tickets_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class mCustomOnClickListener implements AdapterView.OnItemClickListener{

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            String selectedValue = (String) parent.getItemAtPosition(position);
            Long selTicketKey = 0L;
            for(Map.Entry<Long,Ticket> mEntry: tickets.entrySet()){
                if(String.valueOf(mEntry.getValue().getTicketNr()).equals(selectedValue)){
                    selTicketKey = mEntry.getKey();
                }
            }
            if (selTicketKey>0){
                Intent intent = new Intent(getApplicationContext(), TicketQRActivity.class);
                intent.putExtra("ticket_number", tickets.get(selTicketKey).getTicketNr());
                intent.putExtra("ticket_customer_id", tickets.get(selTicketKey).getCustomer_id());
                intent.putExtra("ticket_data", tickets.get(selTicketKey).getData());
                intent.putExtra("caller","tickets_list");
                startActivity(intent);
            }
        }
    }
}
