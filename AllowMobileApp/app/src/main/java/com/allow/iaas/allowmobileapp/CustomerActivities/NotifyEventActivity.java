package com.allow.iaas.allowmobileapp.CustomerActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.Wsdl2Code.WebServices.GCMproxy.GCMproxy;
import com.Wsdl2Code.WebServices.GCMproxy.NotifyInfoType;
import com.Wsdl2Code.WebServices.context.IWsdl2CodeEvents;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
import com.allow.iaas.allowmobileapp.helpers.RoutesAdapter;

import java.util.List;

import com.Wsdl2Code.WebServices.PassengerService.*;

public class NotifyEventActivity extends Activity implements IWsdl2CodeEvents{

    NotifyInfoType notification;
    ListView myListView;
    TextView eventIDview;
    GCMproxy gcmService;
    String user_id, event_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notify_event);
        Intent mIntent = getIntent();
        String user_id =mIntent.getStringExtra("user_id");
        String event_id =mIntent.getStringExtra("event_id");


        myListView = (ListView) findViewById(R.id.listView_notifyEvent);
        eventIDview = (TextView) findViewById(R.id.textView_eventId);
        gcmService = new GCMproxy(this);
        gcmService.setUrl(MyConstants.ENDPOINT_GCM);
        try {
            gcmService.GetNotifyEventInfoAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_notify_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void Wsdl2CodeStartedRequest() {

    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object Data) {

        switch(methodName){
            case "GetNotifyEventInfo":
                NotifyInfoType mInfo = (NotifyInfoType)Data;
                eventIDview.setText(mInfo.eventID);

                int numberOfRoutes = mInfo.tripAlternatives.size();
                List<tripAlternativeType> alternatives = RoutesAdapter.VectorToList(mInfo.tripAlternatives);
                RoutesAdapter mAlternativesAdapter = new RoutesAdapter(this, alternatives);
                myListView.setAdapter(mAlternativesAdapter);

                myListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        tripAlternativeType myTripAlternative = (tripAlternativeType) parent.getItemAtPosition(position);
                        Toast.makeText(getApplicationContext(), myTripAlternative.route.getRouteSummary(), Toast.LENGTH_SHORT).show();

                        try {
                            gcmService.SendEventAsync("trip_alternatives","ticket_id", myTripAlternative);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });


                break;

        }

    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {

    }

    @Override
    public void Wsdl2CodeEndedRequest() {

    }
}
