package com.allow.iaas.allowmobileapp.CustomerActivities;

import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.Wsdl2Code.WebServices.GCMproxy.GCMproxy;
import com.Wsdl2Code.WebServices.PassengerService.PassengerPassengerPTService;
import com.Wsdl2Code.WebServices.context.IWsdl2CodeEvents;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
import com.allow.iaas.allowmobileapp.helpers.barcode.QRCodeView;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.database.Ticket;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;

import java.util.Map;

public class TicketQRActivity extends Activity {

    QRCodeView CodeView;
    private TextView tVticketName, tVticketNumber;
    protected mySQLiteHelper db;
    long TicketCustomer_id;

    String passengerUUID = null;
    String routeID = null;
    String passengerName = null;
    long passenger_id = 0;

    @Override
    protected void onPause() {
        super.onPause();
        db.closeDB();
    }

    @Override
    protected void onResume(){
        super.onResume();
        db  = new mySQLiteHelper(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_ticket_qr);

        CodeView = (QRCodeView) findViewById(R.id.QRCodeView);
        tVticketName = (TextView) findViewById(R.id.textView_Customer);
        tVticketNumber = (TextView) findViewById(R.id.textView_ticketNr);
        Intent mIntent = getIntent();
        int TickerNumber = 0;
        TicketCustomer_id = 0L;
        String TicketData = null;

        this.passengerUUID = mIntent.getStringExtra("passengerUUID");
        this.routeID = mIntent.getStringExtra("routeID");
        this.passengerName = mIntent.getStringExtra("passengerName");
        this.passenger_id = mIntent.getLongExtra("passenger_id", 0);
        TicketData = mIntent.getStringExtra("ticket_data");
        TickerNumber = mIntent.getIntExtra("ticket_number", 0);
        TicketCustomer_id = mIntent.getLongExtra("ticket_customer_id", 0);
        String caller =mIntent.getStringExtra("caller");
        if (caller.equals("payment_confirmation")) {
            if(TickerNumber>0 && TicketCustomer_id >0 && TicketData.length()>0){
                db  = new mySQLiteHelper(getApplicationContext());
                long pKey = db.getNewPrimaryKey(mySQLiteHelper.TABLE_PURCHASED_TICKETS);
                db.createPurchasedTicket(pKey, TickerNumber, TicketCustomer_id, TicketData);
            }

            PassengerPassengerPTService passService = new PassengerPassengerPTService(
                    new com.Wsdl2Code.WebServices.PassengerService.IWsdl2CodeEvents() {
                        @Override
                        public void Wsdl2CodeStartedRequest() {

                        }

                        @Override
                        public void Wsdl2CodeFinished(String methodName, Object Data) {
                            switch (methodName) {
                                case "receiveTransactionStatus":
                                    System.out.println("Received Transaction Status");
                                    Toast.makeText(getApplicationContext(), "Notified Transaction Status...", Toast.LENGTH_SHORT).show();
                                    break;
                            }
                        }

                        @Override
                        public void Wsdl2CodeFinishedWithException(Exception ex) {

                        }

                        @Override
                        public void Wsdl2CodeEndedRequest() {

                        }
                    }


            );

            try {
                passService.setUrl(MyConstants.ENDPOINT_PASSENGER);
                passService.setTimeOut(10000);
                passService.receiveTransactionStatusAsync("tripBooking", this.passengerUUID, "ok", TicketData);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else if (caller.equals("tickets_list")){
            TickerNumber = mIntent.getIntExtra("ticket_number", 0);
            TicketCustomer_id = mIntent.getLongExtra("ticket_customer_id", 0);
            TicketData = mIntent.getStringExtra("ticket_data");
        }else if (caller.equals("request_ticket")){
            TicketCustomer_id = mIntent.getLongExtra("ticket_customer_id", 0);
        }

        if(TickerNumber>0 && TicketCustomer_id >0 && TicketData.length()>0){
            Map<String, String> CustomerDetails = db.getCustomerDetails(TicketCustomer_id);
            if (CustomerDetails.size()>0){
                if (this.routeID != null) {
                    CodeView.setQRcode(TicketCustomer_id +":" + String.valueOf(TickerNumber) + "Route ID: " + this.routeID);
                }
                else {
                    CodeView.setQRcode(TicketCustomer_id +":" + String.valueOf(TickerNumber));
                }

                tVticketName.setText(this.passengerUUID + " - " + CustomerDetails.get(mySQLiteHelper.COLUMN_CUSTOMER_NAME) + " " + CustomerDetails.get(mySQLiteHelper.COLUMN_CUSTOMER_SURNAME));
                tVticketNumber.setText(this.routeID + " - " + String.valueOf(TickerNumber));

            }
        }else if(caller.equals("request_ticket")){
            GCMproxy gcmService = new GCMproxy(new IWsdl2CodeEvents() {
                @Override
                public void Wsdl2CodeStartedRequest() {

                }

                @Override
                public void Wsdl2CodeFinished(String methodName, Object Data) {
                    finish();
                }

                @Override
                public void Wsdl2CodeFinishedWithException(Exception ex) {

                }

                @Override
                public void Wsdl2CodeEndedRequest() {
                    finish();
                }
            });
            Ticket lastTicket = db.getLastTicket(TicketCustomer_id);
            gcmService.setUrl(MyConstants.ENDPOINT_GCM);
            if (lastTicket != null){
                try {
                    gcmService.SendTicketAsync(String.valueOf(lastTicket.getCustomer_id()),
                            String.valueOf(lastTicket.getTicketNr()));
                } catch (Exception e) {
                    e.printStackTrace();
                }}
        }else{
            finish();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ticket_qr, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
