package com.allow.iaas.allowmobileapp.helpers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.Wsdl2Code.WebServices.PassengerService.tripAlternativeType;
import com.allow.iaas.allowmobileapp.R;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

/**
 * Created by julian on 7/19/15.
 */
public class RoutesAdapter extends ArrayAdapter {
    private final Context context;
    private final List<tripAlternativeType> tripAlternativesList;
    private Vector<tripAlternativeType> tripAlternativesVector;


    public RoutesAdapter(Context context, List<tripAlternativeType> tripAlternativesList) {
        super(context, R.layout.target_item, tripAlternativesList);
        this.context = context;
        this.tripAlternativesList = tripAlternativesList;
    }


    /**
     * Converts the vectorType to ListType, so the vector can be used with the adapter
     * @param inputVector
     * @return
     */
    public static List<tripAlternativeType> VectorToList(Vector<tripAlternativeType> inputVector){
        return inputVector.subList(inputVector.indexOf(inputVector.firstElement()),
                inputVector.indexOf(inputVector.lastElement())+1);
    }

    public static List<tripAlternativeType> orderListByUtility (List<tripAlternativeType> list) {
        List<tripAlternativeType> listReturn = list;

        Comparator<tripAlternativeType> comparator = new Comparator<tripAlternativeType>() {
            @Override
            public int compare(tripAlternativeType lhs, tripAlternativeType rhs) {
                if (lhs.utility < rhs.utility) return 1;
                if (lhs.utility == rhs.utility) return 0;
                return -1;
            }
        };
        Collections.sort(listReturn, comparator);
        return listReturn;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // 1. Create inflater
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // 2. Get rowView from inflater
        View rowView = null;
        rowView = inflater.inflate(R.layout.target_item, parent, false);

        //final ViewHolder vh;

        // 3. Get icon,title & counter views from the rowView
        TextView idView = (TextView) rowView.findViewById(R.id.item_title);
        TextView titleView = (TextView) rowView.findViewById(R.id.item_description);
        //TextView counterView = (TextView) rowView.findViewById(R.id.item_counter);

        // 4. Set the text for textView
        idView.setText(String.valueOf(tripAlternativesList.get(position).routeID));
        titleView.setText("RouteUtility: " + new BigDecimal(tripAlternativesList.get(position).utility).setScale(4,BigDecimal.ROUND_HALF_EVEN).toString() + " - " + tripAlternativesList.get(position).route.getRouteSummary());

        // 5. return rowView
        return rowView;
    }

}