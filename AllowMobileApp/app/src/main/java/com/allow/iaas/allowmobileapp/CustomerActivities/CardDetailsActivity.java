package com.allow.iaas.allowmobileapp.CustomerActivities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;

import java.util.Map;


public class CardDetailsActivity extends Activity {

    public static final int CARD_CREATE = 1 ;
    public static final int CARD_UPDATE = 2 ;
    public static final int CARD_EDIT_NEW = 3 ;

    public static final int RESULT_CODE_DELETE_CARD= 1 ;
    public static final int RESULT_CODE_CREATE_CARD_OF_NEW_USER = 2 ;
    public static final int RESULT_CODE_EDIT_CARD_OF_NEW_USER = 3 ;





    public static final String CARDHOLDER = "cardholder";
    public static final String CARDNUMBER = "cardNumber";
    public static final String CVV = "cvv";
    public static final String ADDRESS = "address";
    public static final String CITY = "city";
    public static final String ZIP = "zip";
    public static final String ALIAS ="alias";
    public static final String TYPE ="type";



    protected mySQLiteHelper db;
    private EditText eTcarholderName, eTcardNumber, eTcvv, eTstreetAndNr, eTcity, eTzip, eTalias;
    private Button cardButton, deleteCardButton;
    private Spinner spinnerCardType;

    long CardID;
    int operation, user_type;
    int indexEdit;
    String old_alias;
    long customer_id;

    @Override
    protected void onResume() {
        super.onResume();
        db  = new mySQLiteHelper(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.closeDB();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_card_details);
        db = new mySQLiteHelper(getApplicationContext());
        eTcarholderName = (EditText) findViewById(R.id.editText_CardholderName);
        eTcardNumber = (EditText) findViewById(R.id.editText_CardNumber);
        eTcvv = (EditText) findViewById(R.id.editText_CVV);
        eTstreetAndNr = (EditText) findViewById(R.id.editText_AddressCard);
        eTcity = (EditText) findViewById(R.id.editTextCityCard);
        eTzip = (EditText) findViewById(R.id.editTextZIPcard);
        eTalias = (EditText) findViewById(R.id.editTextAlias);
        cardButton = (Button) findViewById(R.id.button_CreditCard);
        deleteCardButton = (Button) findViewById(R.id.button_Delete_Card);
        spinnerCardType = (Spinner) findViewById(R.id.spinner_card_type);

        operation = getIntent().getIntExtra("operation",CARD_CREATE);
        user_type = getIntent().getIntExtra("user-type",CustomerSignUpActivity.NEW_USER);

        //If operation is Update the information is retreieved from the Database

        String[] paymentTypes = {"Visa", "Mastercard", "AmericanExpress"};
        ArrayAdapter mAdapPayment = new ArrayAdapter(this,android.R.layout.simple_list_item_1,paymentTypes);
        spinnerCardType.setAdapter(mAdapPayment);


        if (operation == CARD_UPDATE){
            user_type = getIntent().getIntExtra("user-type",-1);
            old_alias =getIntent().getStringExtra("old_alias");
            customer_id = getIntent().getLongExtra("customer_id", -1);
            CardID = db.getPrimaryKeyForCustomerCreditCard(old_alias, customer_id);
            cardButton.setText("Update");
            Map<String, String> data = db.getCreditCardDetails(CardID);
            if (data !=null){
                eTcarholderName.setText(data.get(mySQLiteHelper.COLUMN_CARD_CARDHOLDER_NAME));
                eTcardNumber.setText(data.get(mySQLiteHelper.COLUMN_CARD_CARDNUMBER));
                eTcvv.setText(data.get(mySQLiteHelper.COLUMN_CARD_CVV));
                eTstreetAndNr.setText(data.get(mySQLiteHelper.COLUMN_CARD_ADDRESS));
                eTcity.setText(data.get(mySQLiteHelper.COLUMN_CARD_CITY));
                eTzip.setText(data.get(mySQLiteHelper.COLUMN_CARD_ZIP));
                eTalias.setText(data.get(mySQLiteHelper.COLUMN_CARD_ALIAS));
                String CardType = data.get(mySQLiteHelper.COLUMN_CARD_TYPE);
                if(CardType.equals(paymentTypes[0])){
                    spinnerCardType.setSelection(0);
                }else if(CardType.equals(paymentTypes[1])){
                    spinnerCardType.setSelection(1);
                }else if((CardType.equals(paymentTypes[2]))){
                    spinnerCardType.setSelection(2);
                }
            }
        }else if(operation == CARD_CREATE){
            deleteCardButton.setVisibility(View.GONE);
            user_type = getIntent().getIntExtra("user-type",-1);
            if(user_type == CustomerSignUpActivity.EXISTENT_USER){
                customer_id =getIntent().getLongExtra("customer_id", -1);
            }
            cardButton.setText("Create");
        }else if(operation == CARD_EDIT_NEW){
            Intent mIntent = getIntent();
            customer_id = mIntent.getLongExtra("customer_id",-1);
            user_type = getIntent().getIntExtra("user-type",-1);
            old_alias =getIntent().getStringExtra("old_alias");

            Bundle data = mIntent.getExtras();
            eTcarholderName.setText(data.getString("Cardholder"));
            eTcardNumber.setText( String.valueOf(data.getInt("CardNumber")));
            eTcvv.setText(String.valueOf(data.getInt("CVV")));
            eTstreetAndNr.setText(data.getString("Address"));
            eTcity.setText(data.getString("City"));
            eTzip.setText(String.valueOf(data.getInt("ZIP")));
            eTalias.setText(data.getString("alias"));
            String CardType = data.getString("type");
            if(CardType.equals(paymentTypes[0])){
                spinnerCardType.setSelection(0);
            }else if(CardType.equals(paymentTypes[1])){
                spinnerCardType.setSelection(1);
            }else if((CardType.equals(paymentTypes[2]))){
                spinnerCardType.setSelection(2);
            }

            cardButton.setText("Update");
        }

    }



    public void onButtonPressed(View view){
        //TODO perform checking if the information is complete
        if (eTcarholderName.length()<=0){
            eTcarholderName.requestFocus();
            eTcarholderName.setError("The field is empty");
            return;
        }
        if (eTcardNumber.length()<=0){
            eTcardNumber.requestFocus();
            eTcardNumber.setError("The field is empty");
            return;
        }
        if (eTcvv.length()<=0){
            eTcvv.requestFocus();
            eTcvv.setError("The field is empty");
            return;
        }
        if (eTstreetAndNr.length()<=0){
            eTstreetAndNr.requestFocus();
            eTstreetAndNr.setError("The field is empty");
            return;
        }
        if (eTcity.length()<=0){
            eTcity.requestFocus();
            eTcity.setError("The field is empty");
            return;
        }
        if (eTzip.length()<=0){
            eTzip.requestFocus();
            eTzip.setError("The field is empty");
            return;
        }
        if (eTalias.length()<=0){
            eTalias.requestFocus();
            eTalias.setError("The field is empty");
            return;
        }

        String Cardholder =eTcarholderName.getText().toString();
        int CardNumber= Integer.parseInt(eTcardNumber.getText().toString());
        int Cvv=Integer.parseInt(eTcvv.getText().toString());
        String Address=eTstreetAndNr.getText().toString();
        String City=eTcity.getText().toString();
        int zip=Integer.parseInt(eTzip.getText().toString());
        String alias=eTalias.getText().toString();
        String type = spinnerCardType.getSelectedItem().toString();


        if (operation == CARD_UPDATE){

            db.updateCreditCard(CardID,Cardholder,CardNumber, Cvv, Address, City, zip, alias, type);
            finish();


        }else if(operation == CARD_CREATE){


            switch (user_type){
                case CustomerSignUpActivity.NEW_USER:
                    Intent mIntent = new Intent();
                    mIntent.putExtra(CARDHOLDER, Cardholder);
                    mIntent.putExtra(CARDNUMBER, CardNumber);
                    mIntent.putExtra(CVV, Cvv);
                    mIntent.putExtra(ADDRESS, Address);
                    mIntent.putExtra(CITY, City);
                    mIntent.putExtra(ZIP, zip);
                    mIntent.putExtra(ALIAS, alias);
                    mIntent.putExtra(TYPE, type);
                    setResult(RESULT_CODE_CREATE_CARD_OF_NEW_USER,mIntent);
                    break;
                case CustomerSignUpActivity.EXISTENT_USER:
                    long newCardKey =db.getNewPrimaryKey(mySQLiteHelper.TABLE_CREDIT_CARDS);
                    if(customer_id>0){
                        db.createCreditCard(newCardKey,customer_id,Cardholder,CardNumber,Cvv,Address,City,zip,alias,type);
                    }
                    break;
            }

            finish();
        }else if(operation == CARD_EDIT_NEW){
            Intent mIntent = new Intent();

            mIntent.putExtra(CARDHOLDER, Cardholder);
            mIntent.putExtra(CARDNUMBER, CardNumber);
            mIntent.putExtra(CVV, Cvv);
            mIntent.putExtra(ADDRESS, Address);
            mIntent.putExtra(CITY, City);
            mIntent.putExtra(ZIP, zip);
            mIntent.putExtra(ALIAS, alias);
            mIntent.putExtra("old_alias", old_alias);
            setResult(RESULT_CODE_EDIT_CARD_OF_NEW_USER,mIntent);
            finish();
        }

    }

    public void onDeleteClick(View view){
        if(operation== CARD_UPDATE){
            if (user_type == CustomerSignUpActivity.EXISTENT_USER) {
                if (customer_id > 0) {
                    db.deleteCreditCard(CardID);
                }
            }
            finish();
        }else if (operation == CARD_EDIT_NEW){
            Intent mIntent = new Intent();
            mIntent.putExtra("old_alias", old_alias);
            setResult(RESULT_CODE_DELETE_CARD,mIntent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_card_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
