package com.allow.iaas.allowmobileapp.BusActivities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.Wsdl2Code.WebServices.RouteManager.RouteManager;
import com.Wsdl2Code.WebServices.context.IWsdl2CodeEvents;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
import com.allow.iaas.allowmobileapp.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class BusMapsActivity extends FragmentActivity  implements LocationListener, IWsdl2CodeEvents{

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private LocationManager locationManager;
    private Location myLocation;
    Marker myPositionMarker, nextStopMarker;
    EditText eTincidence;
    View progressBar, formView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_maps);
        eTincidence = (EditText) findViewById(R.id.editText_incidence);
        progressBar = findViewById(R.id.progressBar_BusMap);
        formView = findViewById(R.id.layout_BusMap);
        showProgress(false);
        setUpMapIfNeeded();

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,this);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
    }

    public void onCheckInButtonClick(View view){
        Intent mIntent = new Intent(this, BusPassengerCheckInActivity.class);
        BusMapsActivity.this.startActivity(mIntent);
    }

    public void clickReportIncidence(View view){
        if(eTincidence.getText().toString().length()>0){
            RouteManager routeManagerService = new RouteManager(this);
            routeManagerService.setUrl(MyConstants.ENDPOINT_ROUTE_MANAGER);

            try {
                routeManagerService.ReportIncidenceAsync(eTincidence.getText().toString());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }else{
            eTincidence.setError("Please give an incidence");
            eTincidence.requestFocus();
        }
    }
    public void changeMapType (View view){
        int  mapType =  mMap.getMapType();
        switch(mapType){

            case GoogleMap.MAP_TYPE_NORMAL:
                mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                break;
            case GoogleMap.MAP_TYPE_SATELLITE:
                mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                break;
            case GoogleMap.MAP_TYPE_TERRAIN:
                mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                break;
            case GoogleMap.MAP_TYPE_HYBRID:
                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                break;
        }
    }

    public void getNextStop (View view){

        RouteManager routeManagerService = new RouteManager(this);
        routeManagerService.setUrl(MyConstants.ENDPOINT_ROUTE_MANAGER);
        com.Wsdl2Code.WebServices.RouteManager.Location tmpLoc = new com.Wsdl2Code.WebServices.RouteManager.Location();
        tmpLoc.latitude = myLocation.getLatitude();
        tmpLoc.longitude = myLocation.getLongitude();
        if (nextStopMarker != null){
            nextStopMarker.remove();
        }
        try {
            routeManagerService.GetNextStopAsync(tmpLoc);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*******************************
     * Implements LocationListener
     *
     ********************************/

    @Override
    public void onLocationChanged(Location location) {
        myLocation = location;
        if(location.getAccuracy() <60){
            if(myPositionMarker != null){
                myPositionMarker.remove();
            }
            if(mMap !=null){
                myPositionMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(), location.getLongitude()))
                        .title("Current Location"));
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    /**
     * ****************************************
     */
    @Override
    public void Wsdl2CodeStartedRequest() {
        showProgress(true);
    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object Data) {
            switch (methodName){
                case "GetNextStop":
                    com.Wsdl2Code.WebServices.RouteManager.Location nextStop =  (com.Wsdl2Code.WebServices.RouteManager.Location) Data;
                    nextStopMarker = mMap.addMarker(new MarkerOptions().position(new LatLng(nextStop.latitude, nextStop.longitude))
                            .title("Next Stop"));
                    break;

            }
    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {

    }

    @Override
    public void Wsdl2CodeEndedRequest() {
        showProgress(false);
    }


    /**
     * Shows the progress UI and hides the login form, during the ProgressBar Animation
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            // if (false) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
            formView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    formView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            progressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }
}
