package com.allow.iaas.allowmobileapp.helpers.database;

/**
 * Created by julian on 6/21/15.
 */
public class Ticket {



    private Long Customer_id;
    private String Data;
    private int TicketNr;

    public Ticket(Long customer_id, String data, int ticketNr) {
        Customer_id = customer_id;
        Data = data;
        TicketNr = ticketNr;
    }

    public Long getCustomer_id() {
        return Customer_id;
    }

    public void setCustomer_id(Long customer_id) {
        Customer_id = customer_id;
    }

    public String getData() {
        return Data;
    }

    public void setData(String data) {
        Data = data;
    }

    public int getTicketNr() {
        return TicketNr;
    }

    public void setTicketNr(int ticketNr) {
        TicketNr = ticketNr;
    }
}
