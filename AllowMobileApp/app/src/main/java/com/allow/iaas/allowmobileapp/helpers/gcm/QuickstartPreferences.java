package com.allow.iaas.allowmobileapp.helpers.gcm;

/**
 * Created by julian on 6/8/15.
 */
public class QuickstartPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

}
