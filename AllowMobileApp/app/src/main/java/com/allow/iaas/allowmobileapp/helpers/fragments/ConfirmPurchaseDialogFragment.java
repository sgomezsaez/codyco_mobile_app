package com.allow.iaas.allowmobileapp.helpers.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.allow.iaas.allowmobileapp.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ConfirmPurchaseDialogFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ConfirmPurchaseDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConfirmPurchaseDialogFragment extends DialogFragment {

    private int mPosition;
    private String routeID;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction

        this.mPosition = getArguments().getInt("position");
        this.routeID = getArguments().getString("routeID");



        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Confirm Purchase for route " + getArguments().getString("routeID") + "?")
                .setPositiveButton(R.string.purchase_positive, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        mListener.onBuyClick(mPosition, routeID);
                    }
                })
                .setNegativeButton(R.string.purchase_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                        ConfirmPurchaseDialogFragment.this.getDialog().cancel();
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }


    public interface PurchaseDialogListener {
        public void onBuyClick(int position, String routeID);
    }
    PurchaseDialogListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            mListener = (PurchaseDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

}
