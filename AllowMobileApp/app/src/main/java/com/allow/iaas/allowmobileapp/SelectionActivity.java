package com.allow.iaas.allowmobileapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.allow.iaas.allowmobileapp.BusActivities.BusDriverLoginActivity;
import com.allow.iaas.allowmobileapp.CarPoolingActivities.LoginCarPoolingActivity;
import com.allow.iaas.allowmobileapp.CustomerActivities.LoginCustomerActivity;
import com.allow.iaas.allowmobileapp.TaxiActivities.LoginTaxiDriverActivity;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
import com.allow.iaas.allowmobileapp.helpers.gcm.QuickstartPreferences;
import com.allow.iaas.allowmobileapp.helpers.gcm.RegistrationIntentService;
import com.google.android.gms.common.ConnectionResult;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;


import java.util.concurrent.atomic.AtomicInteger;

public class SelectionActivity extends ActionBarActivity {

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private View mRegistrationProgressBar, mSelectionView;
    AtomicInteger msgId = new AtomicInteger();

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String SENDER_ID = "1093769792435";
    private static final String TAG = "SelectionActivity";
    private GoogleCloudMessaging gcm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        mRegistrationProgressBar = findViewById(R.id.registrationGCMProgressBar);
        mSelectionView =findViewById(R.id.layout_Selection);
        //Get the instance of the Google cloud message Service
        gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                showProgress(false);
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
                boolean sentToken = sharedPreferences.getBoolean(QuickstartPreferences.SENT_TOKEN_TO_SERVER, false);
                if (sentToken) {
                   Toast.makeText(getApplicationContext(),"token received and stored in preferences", Toast.LENGTH_LONG).show();
                    sharedPreferences.edit().putString(getString(R.string.key_preference_token_gcm), sharedPreferences.getString("RegId","none")).apply();
                } else {
                    Toast.makeText(getApplicationContext(),"token error", Toast.LENGTH_LONG).show();
                }
            }
        };

        showProgress(true);
        // If the GCM Services are available in device, then Start IntentService to register this application with GCM.
        if (checkPlayServices()) {
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_selection, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent myIntent = new Intent(this, SettingsEndpointsActivity.class);
            startActivity(myIntent);
            return true;
        }else if(id == R.id.action_about){
            showAboutDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void clickOnCustomer(View view){
        updateEndpointConstants();
        Intent mIntent = new Intent(this, LoginCustomerActivity.class);
        SelectionActivity.this.startActivity(mIntent);
    }

    public void clickOnBus(View view){
        updateEndpointConstants();
        Intent mIntent = new Intent(this, BusDriverLoginActivity.class);
        SelectionActivity.this.startActivity(mIntent);
    }

    public void clickOnTaxi(View view){
        updateEndpointConstants();
        Intent mIntent = new Intent(this, LoginTaxiDriverActivity.class);
        SelectionActivity.this.startActivity(mIntent);
    }

    public void clickOnCarPooling(View view){
        updateEndpointConstants();
        Intent mIntent = new Intent(this, LoginCarPoolingActivity.class);
        SelectionActivity.this.startActivity(mIntent);
    }

    /**
     * Check if the google Services are available
     * @return true if the Google Cloud Services are available, false if not
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }


    /**
     * Shows the progress UI and hides the login form, during the ProgressBar Animation
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            // if (false) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mSelectionView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSelectionView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSelectionView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mRegistrationProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            mRegistrationProgressBar.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mRegistrationProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mRegistrationProgressBar.setVisibility(show ? View.VISIBLE : View.GONE);
            mSelectionView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    private void updateEndpointConstants(){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        MyConstants.ENDPOINT_ROUTE_MANAGER = sharedPreferences.getString(getString(R.string.key_preference_route_manager_endpoint), MyConstants.ENDPOINT_ROUTE_MANAGER);
        MyConstants.ENDPOINT_CONTEXT = sharedPreferences.getString(getString(R.string.key_preference_context_endpoint), MyConstants.ENDPOINT_CONTEXT);
        MyConstants.ENDPOINT_ADAPTATION_ENGINE = sharedPreferences.getString(getString(R.string.key_preference_adaptation_engine_endpoint), MyConstants.ENDPOINT_ADAPTATION_ENGINE);;
        MyConstants.ENDPOINT_GCM = sharedPreferences.getString(getString(R.string.key_preference_google_proxy_endpoint), MyConstants.ENDPOINT_GCM);
        MyConstants.ENDPOINT_PASSENGER = sharedPreferences.getString(getString(R.string.key_preference_passenger_endpoint), MyConstants.ENDPOINT_PASSENGER);
    }

    /**
     * Created to show the About Information
     */
    public void showAboutDialog(){
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        // Setting Dialog Title
        alertDialog.setTitle("About Allow Ensembles");
        // Setting Dialog Message
        alertDialog.setMessage("IAAS Uni Stuttgart \n Ver 0.1a \n Contact: julian.martinez.salazar@gmail.com");
        // Setting Icon to Dialog
        //alertDialog.setIcon(R.drawable.ic_launcher);
        // Setting OK Button
        alertDialog.setButton("Accept", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                // Write your code here to execute after dialog closed
                Toast.makeText(getApplicationContext(), "IAAS 2015", Toast.LENGTH_SHORT).show();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
}
