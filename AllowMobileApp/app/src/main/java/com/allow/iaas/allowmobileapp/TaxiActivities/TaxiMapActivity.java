package com.allow.iaas.allowmobileapp.TaxiActivities;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.Wsdl2Code.WebServices.GCMproxy.GCMproxy;
import com.Wsdl2Code.WebServices.context.IWsdl2CodeEvents;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;

public class TaxiMapActivity extends Activity implements IWsdl2CodeEvents{

    TextView tV_taxiRequest;
    Button button_rejectRequest, button_acceptRequest;
    String PassengerId, caller;
    Double InitLat, InitLong, EndLat, EndLong;
    GCMproxy gcmProxyService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_taxi_map);

        tV_taxiRequest = (TextView) findViewById(R.id.textView_taxiRequest);
        button_rejectRequest = (Button) findViewById(R.id.button_rejectRequest);
        button_acceptRequest = (Button) findViewById(R.id.button_acceptRequest);
        Intent mIntent = getIntent();

        PassengerId =  mIntent.getStringExtra("user_id");
        caller = mIntent.getStringExtra("caller");
        InitLat = Double.parseDouble(mIntent.getStringExtra("init_lat"));
        InitLong = Double.parseDouble(mIntent.getStringExtra("init_long"));
        EndLat = Double.parseDouble(mIntent.getStringExtra("end_lat"));
        EndLong =Double.parseDouble(mIntent.getStringExtra("end_long"));
        gcmProxyService = new GCMproxy(this);
        gcmProxyService.setUrl(MyConstants.ENDPOINT_GCM);

        if (caller.equals("request_taxi_service")){
            tV_taxiRequest.setText("pass: " + PassengerId + " Initial Location: " + InitLat + " ," + InitLong +  " Destiny Location: " + EndLat + " , "  + EndLong );
        }else{
            finish();
        }

    }

    public void acceptTaxiRequest(View view){
        try {
            gcmProxyService.ReceiveTaxiAnswerAsync((Boolean) true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void rejectTaxiRequest(View view){
        try {
            gcmProxyService.ReceiveTaxiAnswerAsync((Boolean) false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_taxi_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void Wsdl2CodeStartedRequest() {

    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object Data) {

    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {

    }

    @Override
    public void Wsdl2CodeEndedRequest() {
        finish();
    }
}
