package com.allow.iaas.allowmobileapp.helpers.barcode;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;

import com.allow.iaas.allowmobileapp.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;


public class QRCodeView extends View {

    private Drawable mEmptyDrawable;
    private Bitmap myBitmap;
    protected Paint bitmapPaint=new Paint();
    Rect QRSize = new Rect();

    public QRCodeView(Context context) {
        super(context);
        init(null, 0);
    }

    public QRCodeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public QRCodeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.QRCodeView, defStyle, 0);



        if (a.hasValue(R.styleable.QRCodeView_QR_EmptyDrawable)) {
            mEmptyDrawable = a.getDrawable(
                    R.styleable.QRCodeView_QR_EmptyDrawable);
            mEmptyDrawable.setCallback(this);
        }

        a.recycle();


    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // TODO: consider storing these as member variables to reduce
        // allocations per draw cycle.
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;



        if (myBitmap != null){
                QRSize.left=0;
                QRSize.top=0;
                QRSize.right =contentWidth;
                QRSize.bottom =contentHeight;

    canvas.drawBitmap(myBitmap,null,QRSize,bitmapPaint);
         //   canvas.drawBitmap(myBitmap, 0, 0, bitmapPaint);
        }else if (mEmptyDrawable != null) {
            mEmptyDrawable.setBounds(paddingLeft, paddingTop,
                    paddingLeft + contentWidth, paddingTop + contentHeight);
            mEmptyDrawable.draw(canvas);
        }
    }


    /**
     * Gets the example drawable attribute value.
     *
     * @return The example drawable attribute value.
     */
    public Drawable getEmptyDrawable() {
        return mEmptyDrawable;
    }

    /**
     * Sets the view's example drawable attribute value. In the example view, this drawable is
     * drawn above the text.
     *
     * @param EmptyDrawable The example drawable attribute value to use.
     */
    public void setEmptyDrawable(Drawable EmptyDrawable) {
        mEmptyDrawable = EmptyDrawable;
    }

    public void addBitmap(Bitmap bitmap){
        myBitmap = bitmap;
        invalidate();
    }

    /*public void addBitmap(){

        Bitmap bm = BitmapFactory.decodeResource(getResources(),R.raw.facebookicon);
        myBitmap =bm;
        invalidate();
    }*/

    public void setQRcode(String data){
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix matrix = writer.encode(
                    data, BarcodeFormat.QR_CODE, 400, 400
            );
            myBitmap = toBitmap(matrix);

        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private  Bitmap toBitmap(BitMatrix matrix){
        int height = matrix.getHeight();
        int width = matrix.getWidth();
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++){
            for (int y = 0; y < height; y++){
                bmp.setPixel(x, y, matrix.get(x,y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }
}
