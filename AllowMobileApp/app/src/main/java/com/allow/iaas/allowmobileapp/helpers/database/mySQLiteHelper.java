package com.allow.iaas.allowmobileapp.helpers.database;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.allow.iaas.allowmobileapp.helpers.database.Preference;

/**
 * Created by julian on 3/12/15.
 */
public class mySQLiteHelper extends SQLiteOpenHelper{
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    // Database Version
    public static final int DATABASE_VERSION = 1;
    // Database Name
    public static final String DATABASE_NAME = "AllowsEnsemble.db";

    //Table Names
    public static final String TABLE_CUSTOMERS = "customer_table";
    public static final String TABLE_BUS_DRIVERS = "bus_drivers";
    public static final String TABLE_TAXI_DRIVERS = "taxi_drivers";
    public static final String TABLE_CAR_POOLING_DRIVERS= "car_pooling_drivers";
    public static final String TABLE_CREDIT_CARDS= "credit_cards";
    public static final String TABLE_PURCHASED_TICKETS ="purchased_tickets";
    public static final String TABLE_PREFERENCES_BY_USER ="preferences_by_user";
    //  Common column names
    public static final String KEY_ID = "id";


    //TABLE_CUSTOMERS column names
    public static final String COLUMN_CUSTOMER_NAME = "name";
    public static final String COLUMN_CUSTOMER_SURNAME = "surname";
    public static final String COLUMN_CUSTOMER_CITY = "city";
    public static final String COLUMN_CUSTOMER_STATE= "state";
    public static final String COLUMN_CUSTOMER_USERNAME= "username";
    public static final String COLUMN_CUSTOMER_PASSWORD= "password";
    public static final String COLUMN_CUSTOMER_PREFERRED_TRANSPORT= "preferred_transport";
    public static final String COLUMN_CUSTOMER_ENTITY_ID = "customer_entity_id";
    public static final String COLUMN_CUSTOMER_POLICY_TYPE = "customer_policy_type";

    //TABLE_BUS_DRIVERS
    public static final String COLUMN_BUS_DRIVER_NAME = "driver";
    public static final String COLUMN_BUS_DRIVER_SURNAME= "surname";
    public static final String COLUMN_BUS_DRIVER_ADDRESS= "address";
    public static final String COLUMN_BUS_DRIVER_CITY= "city";
    public static final String COLUMN_BUS_DRIVER_ZIP= "ZIP";
    public static final String COLUMN_BUS_DRIVER_USERNAME= "username";
    public static final String COLUMN_BUS_DRIVER_PASSWORD= "password";
    public static final String COLUMN_BUS_DRIVER_ENTITY_ID= "bus_entity_id";

    //TABLE_TAXI_DRIVERS
    public static final String COLUMN_TAXI_DRIVER_NAME = "driver";
    public static final String COLUMN_TAXI_DRIVER_SURNAME= "surname";
    public static final String COLUMN_TAXI_DRIVER_ADDRESS= "address";
    public static final String COLUMN_TAXI_DRIVER_CITY= "city";
    public static final String COLUMN_TAXI_DRIVER_ZIP= "ZIP";
    public static final String COLUMN_TAXI_DRIVER_USERNAME= "username";
    public static final String COLUMN_TAXI_DRIVER_PASSWORD= "password";
    public static final String COLUMN_TAXI_DRIVER_ENTITY_ID = "taxi_entity_id";

    //TABLE_CAR_POOLING_DRIVERS
    public static final String COLUMN_CAR_POOLING_DRIVER_NAME = "driver";
    public static final String COLUMN_CAR_POOLING_DRIVER_SURNAME= "surname";
    public static final String COLUMN_CAR_POOLING_DRIVER_ADDRESS= "address";
    public static final String COLUMN_CAR_POOLING_DRIVER_CITY= "city";
    public static final String COLUMN_CAR_POOLING_DRIVER_ZIP= "ZIP";
    public static final String COLUMN_CAR_POOLING_DRIVER_USERNAME= "username";
    public static final String COLUMN_CAR_POOLING_DRIVER_PASSWORD= "password";
    public static final String COLUMN_CAR_DRIVER_ENTITY_ID = "car_pool_entity_id";


    //TABLE_CREDIT_CARDS
    public static final String COLUMN_CARD_ALIAS = "alias";
    public static final String COLUMN_CARD_CARDHOLDER_NAME = "name";
    public static final String COLUMN_CARD_CARDNUMBER = "number";
    public static final String COLUMN_CARD_CVV = "cvv";
    public static final String COLUMN_CARD_ADDRESS = "address";
    public static final String COLUMN_CARD_CITY = "city";
    public static final String COLUMN_CARD_ZIP = "ZIP";
    public static final String COLUMN_CARD_TYPE = "type";

    //this one is used by TABLE_CREDIT_CARDS and TABLE_PREFERENCES_BY_USER
    public static final String KEY_CUSTOMER = "id_customer";

    //TABLE_PURCHASED_TICKETS
    public static final String COLUMN_TICKET_NUMBER = "ticketNr";
    public static final String COLUMN_TICKET_CUSTOMER_KEY = "customer";
    public static final String COLUMN_TICKET_DATA= "data";

    //TABLE_PREFERENCES_BY_USER
    public static final String COLUMN_TIME_MAX = "time_max";
    public static final String COLUMN_COST_MAX = "time_max";

    public static final String COLUMN_PREFERENCE_CUSTOMER_ENTITY_ID = "customer_entity_id";
    public static final String COLUMN_PREFERENCEID = "preference_ID";
    public static final String COLUMN_PREFERENCENAME = "preference_Name";
    public static final String COLUMN_PREFERENCEVALUE = "preference_Value";
    public static final String COLUMN_PREFERENCEWEIGHT = "preference_Weight";

    public static final String PREFERENCE_NOCHANGES = "NoOfChanges";
    public static final String PREFERENCE_WALKING_DISTANCE = "WalkingDistance";
    public static final String PREFERENCE_TRAVEL_TIME = "TravelTime";
    public static final String PREFERENCE_COST = "Cost";

    public static final String COLUMN_ESTIM_UTIL = "estim_util";
    public static final String COLUMN_ACTUAL_UTIL= "actual_util";




    // Table Create Statements
    private static final String CREATE_TABLE_CUSTOMERS = "CREATE TABLE " + TABLE_CUSTOMERS + " (" +
            KEY_ID + " LONG PRIMARY KEY," +
            COLUMN_CUSTOMER_NAME + " TEXT," +
            COLUMN_CUSTOMER_SURNAME + " TEXT," +
            COLUMN_CUSTOMER_CITY + " TEXT," +
            COLUMN_CUSTOMER_STATE + " TEXT," +
            COLUMN_CUSTOMER_USERNAME + " TEXT," +
            COLUMN_CUSTOMER_PASSWORD + " TEXT," +
            COLUMN_CUSTOMER_PREFERRED_TRANSPORT + " TEXT," +
            COLUMN_CUSTOMER_ENTITY_ID + " TEXT," +
            COLUMN_CUSTOMER_POLICY_TYPE + " TEXT" +
            " )" ;

    // Table Create Preferences

    private static final String CREATE_TABLE_PREFERENCES = "CREATE TABLE " + TABLE_PREFERENCES_BY_USER + " (" +
            COLUMN_PREFERENCE_CUSTOMER_ENTITY_ID + " LONG," +
            COLUMN_PREFERENCEID + " TEXT," +
            COLUMN_PREFERENCENAME + " TEXT," +
            COLUMN_PREFERENCEVALUE + " DOUBLE," +
            COLUMN_PREFERENCEWEIGHT + " DOUBLE," +
            " FOREIGN KEY(" + COLUMN_PREFERENCE_CUSTOMER_ENTITY_ID + ") REFERENCES " + TABLE_CUSTOMERS + "(" + KEY_ID + ") ON DELETE CASCADE" +
            " )" ;

    private static final String CREATE_TABLE_BUS_DRIVERS= "CREATE TABLE " + TABLE_BUS_DRIVERS + " (" +
            KEY_ID + " LONG PRIMARY KEY," +
            COLUMN_BUS_DRIVER_NAME + " TEXT," +
            COLUMN_BUS_DRIVER_SURNAME + " TEXT," +
            COLUMN_BUS_DRIVER_ADDRESS + " TEXT," +
            COLUMN_BUS_DRIVER_CITY + " TEXT," +
            COLUMN_BUS_DRIVER_ZIP + " INTEGER," +
            COLUMN_BUS_DRIVER_USERNAME + " TEXT," +
            COLUMN_BUS_DRIVER_PASSWORD + " TEXT," +
            COLUMN_BUS_DRIVER_ENTITY_ID + "TEXT" +
            " )" ;

    private static final String CREATE_TABLE_TAXI_DRIVERS= "CREATE TABLE " + TABLE_TAXI_DRIVERS + " (" +
            KEY_ID + " LONG PRIMARY KEY," +
            COLUMN_TAXI_DRIVER_NAME + " TEXT," +
            COLUMN_TAXI_DRIVER_SURNAME + " TEXT," +
            COLUMN_TAXI_DRIVER_ADDRESS + " TEXT," +
            COLUMN_TAXI_DRIVER_CITY + " TEXT," +
            COLUMN_TAXI_DRIVER_ZIP + " INTEGER," +
            COLUMN_TAXI_DRIVER_USERNAME + " TEXT," +
            COLUMN_TAXI_DRIVER_PASSWORD + " TEXT," +
            COLUMN_TAXI_DRIVER_ENTITY_ID + "TEXT" +
            " )" ;



    private static final String CREATE_TABLE_CAR_POOLING_DRIVERS= "CREATE TABLE " + TABLE_CAR_POOLING_DRIVERS + " (" +
            KEY_ID + " LONG PRIMARY KEY," +
            COLUMN_CAR_POOLING_DRIVER_NAME + " TEXT," +
            COLUMN_CAR_POOLING_DRIVER_SURNAME + " TEXT," +
            COLUMN_CAR_POOLING_DRIVER_ADDRESS + " TEXT," +
            COLUMN_CAR_POOLING_DRIVER_CITY + " TEXT," +
            COLUMN_CAR_POOLING_DRIVER_ZIP + " INTEGER," +
            COLUMN_CAR_POOLING_DRIVER_USERNAME + " TEXT," +
            COLUMN_CAR_POOLING_DRIVER_PASSWORD + " TEXT," +
            COLUMN_CAR_DRIVER_ENTITY_ID + "TEXT" +
            " )" ;


    private static final String CREATE_TABLE_CREDIT_CARDS= "CREATE TABLE " + TABLE_CREDIT_CARDS + " (" +
            KEY_ID + " LONG PRIMARY KEY," +
            KEY_CUSTOMER + " LONG," +
            COLUMN_CARD_CARDHOLDER_NAME + " TEXT," +
            COLUMN_CARD_CARDNUMBER + " INTEGER," +
            COLUMN_CARD_CVV + " INTEGER," +
            COLUMN_CARD_ADDRESS + " TEXT," +
            COLUMN_CARD_CITY + " TEXT," +
            COLUMN_CARD_ZIP + " INTEGER," +
            COLUMN_CARD_ALIAS + " TEXT," +
            COLUMN_CARD_TYPE + " TEXT" +
            " )" ;

    private static final String CREATE_TABLE_TICKETS = "CREATE TABLE " + TABLE_PURCHASED_TICKETS + " (" +
            KEY_ID + " LONG PRIMARY KEY," +
            COLUMN_TICKET_NUMBER + " INTEGER," +
            COLUMN_TICKET_CUSTOMER_KEY + " INTEGER," +
            COLUMN_TICKET_DATA + " TEXT" +
            " )" ;


    public mySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_CUSTOMERS);
        db.execSQL(CREATE_TABLE_PREFERENCES);
        db.execSQL(CREATE_TABLE_BUS_DRIVERS);
        db.execSQL(CREATE_TABLE_TAXI_DRIVERS);
        db.execSQL(CREATE_TABLE_CAR_POOLING_DRIVERS);
        db.execSQL(CREATE_TABLE_CREDIT_CARDS);
        db.execSQL(CREATE_TABLE_TICKETS);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CUSTOMERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PREFERENCES_BY_USER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BUS_DRIVERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TAXI_DRIVERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CAR_POOLING_DRIVERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CREDIT_CARDS);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**
     * Returns the Password for the given username
     * @param username
     * @return the password, null if no password was found
     */
    public String findUserPassword(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMERS + " WHERE "
                + COLUMN_CUSTOMER_USERNAME + " = " + username;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            if (c.getCount()>0){
                c.moveToFirst();
                String result =c.getString(c.getColumnIndex(COLUMN_CUSTOMER_PASSWORD));
                c.close();
                return result;}
            else{
                return null;
            }
        }else{
            return null;
        }
    }

    public long findUserTableID(String entityID){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMERS + " WHERE "
                + COLUMN_CUSTOMER_ENTITY_ID + " = \"" + entityID + "\"";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            if (c.getCount()>0){
                c.moveToFirst();
                Long result =c.getLong(c.getColumnIndex(KEY_ID));
                c.close();
                return result;}
            else{
                return -1;
            }
        }else{
            return -1;
        }
    }



    /**
     * Creates customer entry in table
     * @param entityUUID primary key
     * @param name
     * @param surname
     * @param city
     * @param state
     * @param username
     * @param Password
     * @param prefTransport
     * @return Row ID of the newly inserted row, -1 if error an error occurred
     */
    public long createCustomer(String entityUUID, String name, String surname, String city, String state, String username, String Password, String prefTransport, String policyType){
         long key = this.getNewPrimaryKey(TABLE_CUSTOMERS);
         SQLiteDatabase db = this.getWritableDatabase();
         ContentValues values = new ContentValues();
         values.put(KEY_ID,key);
         values.put(COLUMN_CUSTOMER_ENTITY_ID,entityUUID);
         values.put(COLUMN_CUSTOMER_NAME,name);
         values.put(COLUMN_CUSTOMER_SURNAME,surname);
         values.put(COLUMN_CUSTOMER_CITY,city);
         values.put(COLUMN_CUSTOMER_STATE,state);
         values.put(COLUMN_CUSTOMER_USERNAME,username);
         values.put(COLUMN_CUSTOMER_PASSWORD,Password);
         values.put(COLUMN_CUSTOMER_PREFERRED_TRANSPORT, prefTransport);
         values.put(COLUMN_CUSTOMER_POLICY_TYPE, policyType);
         long entry_id = db.insert(TABLE_CUSTOMERS, null, values);
         return key;
    }

    /**
     * * Updates a customer with the given data
     * @param id Its is necessary to update the customer Data
     * @param name
     * @param surname
     * @param city
     * @param state
     * @param username
     * @param Password
     * @param prefTransport
     * @return the number of rows affected, -1 if there the id is incorrect.
     */
    public int updateCustomer(long id, String name, String surname, String city, String state, String username, String Password, String prefTransport, String entityID, String policyType){
        if(id>0){
            SQLiteDatabase db = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_CUSTOMER_NAME,name);
            values.put(COLUMN_CUSTOMER_SURNAME,surname);
            values.put(COLUMN_CUSTOMER_CITY,city);
            values.put(COLUMN_CUSTOMER_STATE,state);
            values.put(COLUMN_CUSTOMER_USERNAME,username);
            values.put(COLUMN_CUSTOMER_PASSWORD,Password);
            values.put(COLUMN_CUSTOMER_PREFERRED_TRANSPORT,prefTransport);
            values.put(COLUMN_CUSTOMER_ENTITY_ID,entityID);
            values.put(COLUMN_CUSTOMER_POLICY_TYPE,policyType);
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.update(TABLE_CUSTOMERS, values, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    public int updateCustomer(String entityID, String name, String surname, String city, String state, String username, String Password, String prefTransport, String policyType){
        long id = this.findUserTableID(entityID);
        return this.updateCustomer(id, name, surname, city, state, username, Password, prefTransport, entityID, policyType);
    }

    /**
     * It is possible to add the EntityID number to the local stored Customer ID
     * @param CustomerId
     * @param EntityID
     * @return
     */
    public int updateCustomerWithEntityID(long CustomerId, String EntityID){
        if(CustomerId>0){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_CUSTOMER_ENTITY_ID,EntityID);
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(CustomerId)};
            int count = db.update(TABLE_CUSTOMERS, values, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    /**
     *
     * @param id
     * @return the number of rows affected if a whereClause is passed in, 0 otherwise. -1 means the id is no valid
     */
    public int deleteCustomer(long id){
        if(id>0){
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.delete(TABLE_CUSTOMERS, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    public long insertPreferencesUser (String passengerID, List<Preference> preferences){


        Long passengerIDTable = this.findUserTableID(passengerID);
        SQLiteDatabase db = this.getWritableDatabase();

        if (! preferences.isEmpty() && passengerIDTable != null) {
            for (Iterator<Preference> i = preferences.iterator(); i.hasNext(); ) {
                ContentValues values = new ContentValues();
                values.put(COLUMN_PREFERENCE_CUSTOMER_ENTITY_ID, passengerIDTable);
                Preference preference = i.next();
                values.put(COLUMN_PREFERENCEID, preference.getPreferenceID());
                values.put(COLUMN_PREFERENCENAME, preference.getPreferenceName());
                values.put(COLUMN_PREFERENCEVALUE, preference.getPreferenceValue());
                values.put(COLUMN_PREFERENCEWEIGHT, preference.getPreferenceWeight());
                db.insert(TABLE_PREFERENCES_BY_USER, null, values);
            }
            return passengerIDTable;
        }
        return -1;
    }

    public ArrayList<Preference> getPreferencesUser (String passengerID) {

        Long passengerIDTable = this.findUserTableID(passengerID);
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Preference> preferences = new ArrayList<Preference>();

        String selectQuery = "SELECT * FROM " + TABLE_PREFERENCES_BY_USER + " WHERE " + COLUMN_PREFERENCE_CUSTOMER_ENTITY_ID + "=" + passengerIDTable;
        Log.e(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null) {
            c.moveToFirst();
            int numberOfPreferences = c.getCount();
            if (numberOfPreferences > 0 ) {
                while (numberOfPreferences > 0){
                    Preference preference = new Preference();
                    preference.setPreferenceID(c.getString(c.getColumnIndex(COLUMN_PREFERENCEID)));
                    preference.setPreferenceName(c.getString(c.getColumnIndex(COLUMN_PREFERENCENAME)));
                    preference.setPreferenceValue(c.getDouble(c.getColumnIndex(COLUMN_PREFERENCEVALUE)));
                    preference.setPreferenceWeight(c.getDouble(c.getColumnIndex(COLUMN_PREFERENCEWEIGHT)));
                    preferences.add(preference);
                    c.moveToNext();
                    numberOfPreferences --;
                }
            }
        }
        return preferences;
    }

    public int updatePreferenceUser (String passengerID, List<Preference> preferences) {
        SQLiteDatabase db = this.getReadableDatabase();
        Long passengerIDTable = this.findUserTableID(passengerID);
        int count = -1;

        for (Iterator<Preference> i = preferences.iterator(); i.hasNext(); ) {
            Preference preference = i.next();
            ContentValues values = new ContentValues();
            values.put(COLUMN_PREFERENCEID, preference.getPreferenceID());
            values.put(COLUMN_PREFERENCENAME, preference.getPreferenceName());
            values.put(COLUMN_PREFERENCEVALUE, preference.getPreferenceValue());
            values.put(COLUMN_PREFERENCEWEIGHT, preference.getPreferenceWeight());
            String whereClause = COLUMN_PREFERENCE_CUSTOMER_ENTITY_ID + "= ? AND " + COLUMN_PREFERENCENAME + "= ?";
            String[] whereArgs ={String.valueOf(passengerIDTable), preference.getPreferenceName()};
            count = db.update(TABLE_PREFERENCES_BY_USER, values, whereClause, whereArgs);
        }
        return count;
    }


    /**
     * Creates Driver Entry in table
     * @param name primary key
     * @param name
     * @param surname
     * @param address
     * @param city
     * @param zip
     * @param username
     * @param Password
     * @return Row ID of the newly inserted row, -1 if error an error occurred
     */
    public long createBusDriver(String name, String surname, String address, String city,int zip, String username, String Password, String entityID){
        if (entityID != null){
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(COLUMN_BUS_DRIVER_ENTITY_ID,entityID);
            values.put(COLUMN_BUS_DRIVER_NAME,name);
            values.put(COLUMN_BUS_DRIVER_SURNAME,surname);
            values.put(COLUMN_BUS_DRIVER_ADDRESS,address);
            values.put(COLUMN_BUS_DRIVER_CITY,city);
            values.put(COLUMN_BUS_DRIVER_ZIP,zip);
            values.put(COLUMN_BUS_DRIVER_USERNAME,username);
            values.put(COLUMN_BUS_DRIVER_PASSWORD,Password);
            long entry_id =-1L;
            try{
                entry_id = db.insert(TABLE_BUS_DRIVERS, null, values);
            }catch(Exception e){
                Log.e(LOG,"",e);
            }finally{
                return entry_id;
            }
        }else {
            return -1;
        }
    }

    public long findBusDriverTableID(String entityID){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BUS_DRIVERS + " WHERE "
                + COLUMN_BUS_DRIVER_ENTITY_ID + " = \" " + entityID + "\" ;";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            if (c.getCount()>0){
                c.moveToFirst();
                Long result =c.getLong(c.getColumnIndex(KEY_ID));
                c.close();
                return result;}
            else{
                return -1;
            }
        }else{
            return -1;
        }
    }

    /**
     * Updates the values of a Bus Driver entry
     * @param id of the BusDriver to Update
     * @param name
     * @param surname
     * @param address
     * @param state
     * @param zip
     * @param username
     * @param Password
     * @return the number of rows affected. -1 means the id is no valid
     */
    public int updateBusDriver(long id, String name, String surname, String address, String state,int zip, String username, String Password, String entityID){
        if (id>0){
            SQLiteDatabase db = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_BUS_DRIVER_NAME,name);
            values.put(COLUMN_BUS_DRIVER_SURNAME,surname);
            values.put(COLUMN_BUS_DRIVER_ADDRESS,address);
            values.put(COLUMN_BUS_DRIVER_CITY,state);
            values.put(COLUMN_BUS_DRIVER_ZIP,zip);
            values.put(COLUMN_BUS_DRIVER_USERNAME,username);
            values.put(COLUMN_BUS_DRIVER_PASSWORD,Password);
            if (entityID != null)
                values.put(COLUMN_BUS_DRIVER_ENTITY_ID,entityID);
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.update(TABLE_BUS_DRIVERS, values, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    public int updateBusDriver(String entityID, String name, String surname, String address, String state,int zip, String username, String Password){
        long busDriverID = this.findBusDriverTableID(entityID);
        return this.updateBusDriver(busDriverID, name, surname, address, state, zip, username, Password, entityID);
    }

    /**
     * Delete the referenced Bus Driver
     * @param id of the Bus Driver
     * @return the number of rows affected if a whereClause is passed in, 0 otherwise. -1 means the id is no valid
     */
    public int deleteBusDriver(long id){
        if(id>0){
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.delete(TABLE_BUS_DRIVERS, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }


    /**
     * Creates a Taxi Driver entry in the table
     * @param entityID
     * @param name
     * @param surname
     * @param address
     * @param state
     * @param zip
     * @param username
     * @param Password
     * @return Row ID of the newly inserted row, -1 if error an error occurred
     */
    public long createTaxiDriver(String entityID, String name, String surname, String address, String state,String zip, String username, String Password){
        if (entityID != null){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_TAXI_DRIVER_ENTITY_ID,entityID);
            values.put(COLUMN_TAXI_DRIVER_NAME,name);
            values.put(COLUMN_TAXI_DRIVER_SURNAME,surname);
            values.put(COLUMN_TAXI_DRIVER_ADDRESS,address);
            values.put(COLUMN_TAXI_DRIVER_CITY,state);
            values.put(COLUMN_TAXI_DRIVER_ZIP,zip);
            values.put(COLUMN_TAXI_DRIVER_USERNAME,username);
            values.put(COLUMN_TAXI_DRIVER_PASSWORD,Password);
            long entry_id = db.insert(TABLE_TAXI_DRIVERS, null, values);
            return entry_id;
        }else
        {
            return -1;
        }
    }

    public long findTaxiDriverTableID(String entityID){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_TAXI_DRIVERS + " WHERE "
                + COLUMN_TAXI_DRIVER_ENTITY_ID + " = \"" + entityID + "\";";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            if (c.getCount()>0){
                c.moveToFirst();
                Long result =c.getLong(c.getColumnIndex(KEY_ID));
                c.close();
                return result;}
            else{
                return -1;
            }
        }else{
            return -1;
        }
    }

    /**
     *Updates the values of a Taxi Driver entry
     * @param id
     * @param name
     * @param surname
     * @param address
     * @param state
     * @param zip
     * @param username
     * @param Password
     * @return the number of rows affected. -1 means the id is no valid
     */
    public int updateTaxiDriver(long id, String name, String surname, String address, String state,String zip, String username, String Password, String entityID){
        if (id>0){
            SQLiteDatabase db = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_TAXI_DRIVER_NAME,name);
            values.put(COLUMN_TAXI_DRIVER_SURNAME,surname);
            values.put(COLUMN_TAXI_DRIVER_ADDRESS,address);
            values.put(COLUMN_TAXI_DRIVER_CITY,state);
            values.put(COLUMN_TAXI_DRIVER_ZIP,zip);
            values.put(COLUMN_TAXI_DRIVER_USERNAME,username);
            values.put(COLUMN_TAXI_DRIVER_PASSWORD,Password);
            values.put(COLUMN_TAXI_DRIVER_ENTITY_ID, entityID);
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.update(TABLE_TAXI_DRIVERS, values, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    public int updateTaxiDriver(String entityID, String name, String surname, String address, String state,String zip, String username, String Password){
        long taxiDriverID = this.findTaxiDriverTableID(entityID);
        return this.updateTaxiDriver(taxiDriverID, name, surname, address, state, zip, username, Password, entityID);
    }


    /**
     * Delete the referenced Taxi Driver
     * @param id of the Taxi Driver
     * @return the number of rows affected if a whereClause is passed in, 0 otherwise. -1 means the id is no valid
     */
    public int deleteTaxiDriver(long id){
        if(id>0){
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.delete(TABLE_TAXI_DRIVERS, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    /**
     *
     * @param entityID
     * @param name
     * @param surname
     * @param address
     * @param state
     * @param zip
     * @param username
     * @param Password
     * @return Row ID of the newly inserted row, -1 if error an error occurred
     */
    public long createCarPoolingiDriver(String entityID, String name, String surname, String address, String state,String zip, String username, String Password){
        if (entityID != null){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_CAR_DRIVER_ENTITY_ID,entityID);
            values.put(COLUMN_CAR_POOLING_DRIVER_NAME,name);
            values.put(COLUMN_CAR_POOLING_DRIVER_SURNAME,surname);
            values.put(COLUMN_CAR_POOLING_DRIVER_ADDRESS,address);
            values.put(COLUMN_CAR_POOLING_DRIVER_CITY,state);
            values.put(COLUMN_CAR_POOLING_DRIVER_ZIP,zip);
            values.put(COLUMN_CAR_POOLING_DRIVER_USERNAME,username);
            values.put(COLUMN_CAR_POOLING_DRIVER_PASSWORD,Password);
            long entry_id = db.insert(TABLE_CAR_POOLING_DRIVERS, null, values);
            return entry_id;
        }else
        {
            return -1;
        }
    }


    public long findCarDriverTableID(String entityID){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CAR_POOLING_DRIVERS + " WHERE "
                + COLUMN_CAR_DRIVER_ENTITY_ID + " = \"" + entityID + "\";";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            if (c.getCount()>0){
                c.moveToFirst();
                Long result =c.getLong(c.getColumnIndex(KEY_ID));
                c.close();
                return result;}
            else{
                return -1;
            }
        }else{
            return -1;
        }
    }
    /**
     *
     * @param id
     * @param name
     * @param surname
     * @param address
     * @param state
     * @param zip
     * @param username
     * @param Password
     * @return the number of rows affected. -1 means the id is no valid
     */
    public int updateCarPoolingiDriver(long id, String name, String surname, String address, String state,String zip, String username, String Password, String entityID){
        if (id>0){
            SQLiteDatabase db = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_CAR_POOLING_DRIVER_NAME,name);
            values.put(COLUMN_CAR_POOLING_DRIVER_SURNAME,surname);
            values.put(COLUMN_CAR_POOLING_DRIVER_ADDRESS,address);
            values.put(COLUMN_CAR_POOLING_DRIVER_CITY,state);
            values.put(COLUMN_CAR_POOLING_DRIVER_ZIP,zip);
            values.put(COLUMN_CAR_POOLING_DRIVER_USERNAME,username);
            values.put(COLUMN_CAR_POOLING_DRIVER_PASSWORD,Password);
            values.put(COLUMN_CAR_DRIVER_ENTITY_ID,entityID);
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.update(TABLE_CAR_POOLING_DRIVERS, values, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    public int updateCarPoolingiDriver(String entityID, String name, String surname, String address, String state,String zip, String username, String Password){
        long carDriverID = this.findCarDriverTableID(entityID);
        return this.updateCarPoolingiDriver(carDriverID, name, surname, address, state, zip, username, Password, entityID);
    }

    /**
     *
     * @param id
     * @return the number of rows affected if a whereClause is passed in, 0 otherwise. -1 means the id is no valid
     */
    public int deleteCarPoolingiDriver(long id){
        if(id>0){
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.delete(TABLE_CAR_POOLING_DRIVERS, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }


    /**
     * Creates a Credit Card entry in the DB
     * @param id
     * @param KeyCustomer
     * @param Cardholder
     * @param CardNumber
     * @param CVV
     * @param Address
     * @param City
     * @param ZIP
     * @param alias
     * @return
     */
    public long createCreditCard(long id, long KeyCustomer, String Cardholder, int CardNumber,
                                 int CVV, String Address, String City, int ZIP, String alias,String type){
        if (id>0){
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_ID,id);
            values.put(KEY_CUSTOMER,KeyCustomer);
            values.put(COLUMN_CARD_CARDHOLDER_NAME,Cardholder);
            values.put(COLUMN_CARD_CARDNUMBER,CardNumber);
            values.put(COLUMN_CARD_CVV,CVV);
            values.put(COLUMN_CARD_ADDRESS,Address);
            values.put(COLUMN_CARD_CITY,City);
            values.put(COLUMN_CARD_ZIP,ZIP);
            values.put(COLUMN_CARD_ALIAS,alias);
            values.put(COLUMN_CARD_TYPE,type);

            long entry_id = db.insert(TABLE_CREDIT_CARDS, null, values);
            return entry_id;
        }else{
            return -1;
        }
    }

    /**
     *
     * @param id Of the Card To Update
     * @param Cardholder
     * @param CardNumber
     * @param CVV
     * @param Address
     * @param City
     * @param ZIP
     * @param alias
     * @return the number of rows affected. -1 means the id is no valid
     */
    public int updateCreditCard(long id, String Cardholder, int CardNumber,
                                int CVV, String Address, String City, int ZIP, String alias, String type){
        if(id>0){
            SQLiteDatabase db = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_CARD_CARDHOLDER_NAME, Cardholder);
            values.put(COLUMN_CARD_CARDNUMBER,CardNumber);
            values.put(COLUMN_CARD_CVV,CVV);
            values.put(COLUMN_CARD_ADDRESS,Address);
            values.put(COLUMN_CARD_CITY,City);
            values.put(COLUMN_CARD_ZIP,ZIP);
            values.put(COLUMN_CARD_ALIAS,alias);
            values.put(COLUMN_CARD_TYPE,type);
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.update(TABLE_CREDIT_CARDS, values, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    /**
     * Delete the referenced Credit Card
     * @param id of the Credit Card
     * @return the number of rows affected if a whereClause is passed in, 0 otherwise.
     * -1 means the id is no valid
     */
    public int deleteCreditCard(long id){
        if(id>0){
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.delete(TABLE_CREDIT_CARDS, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }

    }


    /**
     *
     * @param id
     * @param TicketNr
     * @param CustomerID
     * @param data
     * @return
     */
    public long createPurchasedTicket(long id, int TicketNr, Long CustomerID, String data){
        if (id>0){
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_ID,id);
            values.put(COLUMN_TICKET_NUMBER,TicketNr);
            values.put(COLUMN_TICKET_CUSTOMER_KEY,CustomerID);
            values.put(COLUMN_TICKET_DATA,data);
            long entry_id = db.insert(TABLE_PURCHASED_TICKETS, null, values);
            return entry_id;
        }else{
            return -1;
        }
    }

    public int updatePurchasedTicket(long id, int TicketNr, String Customer, String data){
        if(id>0){
            SQLiteDatabase db = this.getReadableDatabase();
            ContentValues values = new ContentValues();
            values.put(COLUMN_TICKET_NUMBER,TicketNr);
            values.put(COLUMN_TICKET_CUSTOMER_KEY,Customer);
            values.put(COLUMN_TICKET_DATA,data);
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.update(TABLE_PURCHASED_TICKETS, values, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }


    public int deletePurchasedTicket(long id){
        if(id>0){
            SQLiteDatabase db = this.getWritableDatabase();
            String whereClause = KEY_ID + "= ?";
            String[] whereArgs ={String.valueOf(id)};
            int count = db.delete(TABLE_PURCHASED_TICKETS, whereClause, whereArgs);
            return count;
        }else{
            return -1;
        }
    }

    public Map<Long,Ticket> getPurchasedTicketsList(long cust_id){
        Map<Long,Ticket> map = new HashMap<Long, Ticket>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PURCHASED_TICKETS + " WHERE " +
                COLUMN_TICKET_CUSTOMER_KEY  + " = " + cust_id ;
        Log.e(LOG, selectQuery);
        String[] TicketsList=null;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()>0){
            c.moveToFirst();
            while (!c.isLast()){
                Long mKey = c.getLong(c.getColumnIndex(KEY_ID));
                int mTicketNr = c.getInt(c.getColumnIndex(COLUMN_TICKET_NUMBER));
                Long Customer_id = c.getLong(c.getColumnIndex(COLUMN_TICKET_CUSTOMER_KEY));
                String mData = c.getString(c.getColumnIndex(COLUMN_TICKET_DATA));
                map.put(mKey, new Ticket(Customer_id,mData,mTicketNr));
                c.moveToNext();
            }
            if(c.isLast()){
                Long mKey = c.getLong(c.getColumnIndex(KEY_ID));
                int mTicketNr = c.getInt(c.getColumnIndex(COLUMN_TICKET_NUMBER));
                Long Customer_id = c.getLong(c.getColumnIndex(COLUMN_TICKET_CUSTOMER_KEY));
                String mData = c.getString(c.getColumnIndex(COLUMN_TICKET_DATA));
                map.put(mKey, new Ticket(Customer_id,mData,mTicketNr));
            }
            c.close();
            return map;
        }else{
            if( c !=null){
                if (c.isClosed() == false){
                    c.close();
                }
            }
            return map;
        }
    }

    public Map<Long,Ticket> getPurchasedTicketsList(String customerUUID){
        long cust_id = this.getPrimaryKeyForCustomerUUID(customerUUID);
        Map<Long,Ticket> map = new HashMap<Long, Ticket>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PURCHASED_TICKETS + " WHERE " +
                COLUMN_TICKET_CUSTOMER_KEY  + " = " + cust_id ;
        Log.e(LOG, selectQuery);
        String[] TicketsList=null;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()>0){
            c.moveToFirst();
            while (!c.isLast()){
                Long mKey = c.getLong(c.getColumnIndex(KEY_ID));
                int mTicketNr = c.getInt(c.getColumnIndex(COLUMN_TICKET_NUMBER));
                Long Customer_id = c.getLong(c.getColumnIndex(COLUMN_TICKET_CUSTOMER_KEY));
                String mData = c.getString(c.getColumnIndex(COLUMN_TICKET_DATA));
                map.put(mKey, new Ticket(Customer_id,mData,mTicketNr));
                c.moveToNext();
            }
            if(c.isLast()){
                Long mKey = c.getLong(c.getColumnIndex(KEY_ID));
                int mTicketNr = c.getInt(c.getColumnIndex(COLUMN_TICKET_NUMBER));
                Long Customer_id = c.getLong(c.getColumnIndex(COLUMN_TICKET_CUSTOMER_KEY));
                String mData = c.getString(c.getColumnIndex(COLUMN_TICKET_DATA));
                map.put(mKey, new Ticket(Customer_id,mData,mTicketNr));
            }
            c.close();
            return map;
        }else{
            if( c !=null){
                if (c.isClosed() == false){
                    c.close();
                }
            }
            return map;
        }
    }

    public Map<Long,Ticket> getPurchasedTicketsList(){
        Map<Long,Ticket> map = new HashMap<Long, Ticket>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PURCHASED_TICKETS;
        Log.e(LOG, selectQuery);
        String[] TicketsList=null;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()>0){
            c.moveToFirst();
            while (!c.isLast()){
                Long mKey = c.getLong(c.getColumnIndex(KEY_ID));
                int mTicketNr = c.getInt(c.getColumnIndex(COLUMN_TICKET_NUMBER));
                Long Customer_id = c.getLong(c.getColumnIndex(COLUMN_TICKET_CUSTOMER_KEY));
                String mData = c.getString(c.getColumnIndex(COLUMN_TICKET_DATA));
                map.put(mKey, new Ticket(Customer_id,mData,mTicketNr));
                c.moveToNext();
            }
            if(c.isLast()){
                Long mKey = c.getLong(c.getColumnIndex(KEY_ID));
                int mTicketNr = c.getInt(c.getColumnIndex(COLUMN_TICKET_NUMBER));
                Long Customer_id = c.getLong(c.getColumnIndex(COLUMN_TICKET_CUSTOMER_KEY));
                String mData = c.getString(c.getColumnIndex(COLUMN_TICKET_DATA));
                map.put(mKey, new Ticket(Customer_id,mData,mTicketNr));
            }
            c.close();
            return map;
        }else{
            if( c !=null){
                if (c.isClosed() == false){
                    c.close();
                }
            }
            return map;
        }
    }

    /**
     * Retrieves the Last Ticket stored in the phone purchased by a given passenger
     * @param passengerId
     * @return
     */
    public Ticket getLastTicket(Long passengerId){
        Ticket mTicket = null;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_PURCHASED_TICKETS + "WHERE " + COLUMN_TICKET_CUSTOMER_KEY
                + "=" + passengerId +" ORDER BY " + KEY_ID + " LIMIT 1";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()>0){
            c.moveToFirst();
            Long mKey = c.getLong(c.getColumnIndex(KEY_ID));
            int mTicketNr = c.getInt(c.getColumnIndex(COLUMN_TICKET_NUMBER));
            Long Customer_id = c.getLong(c.getColumnIndex(COLUMN_TICKET_CUSTOMER_KEY));
            String mData = c.getString(c.getColumnIndex(COLUMN_TICKET_DATA));
            mTicket = new Ticket(Customer_id, mData, mTicketNr);
            c.close();
            return mTicket;

        }else{
            if( c !=null){
                if (c.isClosed() == false){
                    c.close();
                }
            }
            return mTicket;
        }
    }





    /**
     * Reviews the table and provides a new primary id to create a new entry.
     * @param tableName the name of the table for getting the next primary key
     * @return the Primary Key Value, 1 if there was no previous key
     */
    public long getNewPrimaryKey(String tableName){
        long value = 1;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT MAX("+ KEY_ID +") AS maximum FROM " + tableName;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        int elements = c.getCount();

        if(elements>0  ){
            c.moveToFirst();
            if(!c.isNull(c.getColumnIndex("maximum"))){
                value = c.getLong(c.getColumnIndex("maximum"))+1;
            }
        }
        c.close();
        return value;
    }


    /**
     * Search the current Username-Password keys and returns them
     * @return Username:Password Keys, null if no key on table
     */
    public String[] getUser_Pass_Keys(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMERS;
        Log.e(LOG, selectQuery);
        String[] userKeys=null;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()>0){
            c.moveToFirst();
            int numberOfUsers =  c.getCount();
            userKeys = new String[numberOfUsers];
            int count =0;
            while (!c.isLast()){
                userKeys[count]= c.getString(c.getColumnIndex(COLUMN_CUSTOMER_USERNAME)) +":"+
                        c.getString(c.getColumnIndex(COLUMN_CUSTOMER_PASSWORD));
                count++;
                c.moveToNext();
            }
            if(c.isLast()){
                userKeys[count]= c.getString(c.getColumnIndex(COLUMN_CUSTOMER_USERNAME)) +":"+
                        c.getString(c.getColumnIndex(COLUMN_CUSTOMER_PASSWORD));
            }
            c.close();
            return userKeys;
        }else{
            return userKeys;
        }
    }

    public String[] getTaxi_Pass_Keys(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_TAXI_DRIVERS;
        Log.e(LOG, selectQuery);
        String[] userKeys=null;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()>0){
            c.moveToFirst();
            int numberOfUsers =  c.getCount();
            userKeys = new String[numberOfUsers];
            int count =0;
            while (!c.isLast()){
                userKeys[count]= c.getString(c.getColumnIndex(COLUMN_TAXI_DRIVER_USERNAME)) +":"+
                        c.getString(c.getColumnIndex(COLUMN_TAXI_DRIVER_PASSWORD));
                count++;
                c.moveToNext();
            }
            if(c.isLast()){
                userKeys[count]= c.getString(c.getColumnIndex(COLUMN_TAXI_DRIVER_USERNAME)) +":"+
                        c.getString(c.getColumnIndex(COLUMN_TAXI_DRIVER_PASSWORD));
            }
            c.close();
            return userKeys;
        }else{
            return userKeys;
        }
    }


    public String[] getCarPooling_Pass_Keys(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CAR_POOLING_DRIVERS;
        Log.e(LOG, selectQuery);
        String[] userKeys=null;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()>0){
            c.moveToFirst();
            int numberOfUsers =  c.getCount();
            userKeys = new String[numberOfUsers];
            int count =0;
            while (!c.isLast()){
                userKeys[count]= c.getString(c.getColumnIndex(COLUMN_CAR_POOLING_DRIVER_USERNAME)) +":"+
                        c.getString(c.getColumnIndex(COLUMN_CAR_POOLING_DRIVER_PASSWORD));
                count++;
                c.moveToNext();
            }
            if(c.isLast()){
                userKeys[count]= c.getString(c.getColumnIndex(COLUMN_CAR_POOLING_DRIVER_USERNAME)) +":"+
                        c.getString(c.getColumnIndex(COLUMN_CAR_POOLING_DRIVER_PASSWORD));
            }
            c.close();
            return userKeys;
        }else{
            return userKeys;
        }
    }


    /**
     * Search the current Bus driver Username-Password keys and returns them
     * @return Username:Password Keys, null if no key on table
     */
    public String[] getBusDriver_Pass_Keys(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BUS_DRIVERS;
        Log.e(LOG, selectQuery);
        String[] DriverKeys=null;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.getCount()>0){
            c.moveToFirst();
            int numberOfUsers =  c.getCount();
            DriverKeys = new String[numberOfUsers];
            int count =0;
            while (!c.isLast()){
                DriverKeys[count]= c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_USERNAME)) +":"+
                        c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_PASSWORD));
                count++;
                c.moveToNext();
            }
            if(c.isLast()){
                DriverKeys[count]= c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_USERNAME)) +":"+
                        c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_PASSWORD));
            }
            c.close();
            return DriverKeys;
        }else{
            return DriverKeys;
        }
    }



    /**
     * Get the list of all usernames in the database
     * @return Array of usernames
     */
    public String[] getCustomerUsernames(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMERS;
        Log.e(LOG, selectQuery);
        String[] userNames;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            c.moveToFirst();
            int numberOfUsers =  c.getCount();
            userNames = new String[numberOfUsers];
            int count =0;
            while (!c.isLast()){
                userNames[count]= c.getString(c.getColumnIndex(COLUMN_CUSTOMER_USERNAME));
                count++;
                c.moveToNext();
            }
            if(c.isLast()){
                userNames[count]= c.getString(c.getColumnIndex(COLUMN_CUSTOMER_USERNAME));
            }
            c.close();
            return userNames;
        }else{
            return null;
        }
    }

    public String[] getBusDriverUserNames(){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BUS_DRIVERS;
        Log.e(LOG, selectQuery);
        String[] userNames;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            c.moveToFirst();
            int numberOfUsers =  c.getCount();
            userNames = new String[numberOfUsers];
            int count =0;
            while (!c.isLast()){
                userNames[count]= c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_USERNAME));
                count++;
                c.moveToNext();
            }
            if(c.isLast()){
                userNames[count]= c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_USERNAME));
            }
            c.close();
            return userNames;
        }else{
            return null;
        }
    }

    /**
     * Provides the primary Key for a given username and password
     * @param username
     * @param password
     * @return
     */
    public long getPrimaryKeyForCustomer(String username, String password){
        long value = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMERS+ " WHERE " +COLUMN_CUSTOMER_USERNAME + " = \'" +username +"\'"+
                " AND " + COLUMN_CUSTOMER_PASSWORD + " = \'" +  password +"\'";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        int elements = c.getCount();
        if(elements>0  ){
            c.moveToFirst();
            if(!c.isNull(c.getColumnIndex(KEY_ID))){
                value = c.getLong(c.getColumnIndex(KEY_ID));
            }
        }
        c.close();
        return value;
    }

    public long getPrimaryKeyForCustomerUUID(String customerUUID){
        long value = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMERS+ " WHERE " +COLUMN_CUSTOMER_ENTITY_ID + " = \'" +customerUUID +"\'"+
                ";";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        int elements = c.getCount();
        if(elements>0  ){
            c.moveToFirst();
            if(!c.isNull(c.getColumnIndex(KEY_ID))){
                value = c.getLong(c.getColumnIndex(KEY_ID));
            }
        }
        c.close();
        return value;
    }



    public long getPrimaryKeyForBusDriver(String username, String password){
        long value = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BUS_DRIVERS+ " WHERE " +COLUMN_BUS_DRIVER_USERNAME + " = \'" +username +"\'"+
                " AND " + COLUMN_BUS_DRIVER_PASSWORD + " = \'" +  password +"\'";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        int elements = c.getCount();
        if(elements>0  ){
            c.moveToFirst();
            if(!c.isNull(c.getColumnIndex(KEY_ID))){
                value = c.getLong(c.getColumnIndex(KEY_ID));
            }
        }
        c.close();
        return value;
    }


    /**
     * Provides the Primary Key of a Credit Card by the alias and the customer id
     * @param alias alias given to the credit card
     * @param customer_id primary Key of the customer
     * @return
     */
    public long getPrimaryKeyForCustomerCreditCard(String alias, long customer_id){
        long value = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CREDIT_CARDS+ " WHERE " + COLUMN_CARD_ALIAS + " = \'" +
                alias + "\' AND " + KEY_CUSTOMER + "=" + customer_id ;

        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        int elements = c.getCount();
        if(elements>0  ){
            c.moveToFirst();
            if(!c.isNull(c.getColumnIndex(KEY_ID))){
                value = c.getLong(c.getColumnIndex(KEY_ID));
            }
        }
        c.close();
        return value;
    }

    public long getPrimaryKeyForCustomerCreditCard(String alias, String customerUUID){
        long customer_id = this.getPrimaryKeyForCustomerUUID(customerUUID);
        long value = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CREDIT_CARDS+ " WHERE " + COLUMN_CARD_ALIAS + " = \'" +
                alias + "\' AND " + KEY_CUSTOMER + "=" + customer_id ;

        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        int elements = c.getCount();
        if(elements>0  ){
            c.moveToFirst();
            if(!c.isNull(c.getColumnIndex(KEY_ID))){
                value = c.getLong(c.getColumnIndex(KEY_ID));
            }
        }
        c.close();
        return value;
    }


    public Map<Long, String> getCardsByCustomerIDandType(Long customer_id, String type){
        Map<Long, String> value = new HashMap<Long, String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CREDIT_CARDS+ " WHERE " + COLUMN_CARD_TYPE + " = \'" +
                type + "\' AND " + KEY_CUSTOMER + "=" + customer_id ;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        int elements = c.getCount();
        if(elements>0  ){
            Long mKey = 0L;
            String mType="";
            c.moveToFirst();
            if(!c.isNull(c.getColumnIndex(KEY_ID))){
                mKey = c.getLong(c.getColumnIndex(KEY_ID));
            }
            if(!c.isNull(c.getColumnIndex(COLUMN_CARD_ALIAS))){
                mType = c.getString(c.getColumnIndex(COLUMN_CARD_ALIAS));
                value.put(mKey,mType);
            }
        }
        c.close();
        return value;
    }

    public Map<Long, String> getCardsByCustomerIDandType(String customeruuid, String type){
        long customer_id = this.getPrimaryKeyForCustomerUUID(customeruuid);
        Map<Long, String> value = new HashMap<Long, String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CREDIT_CARDS+ " WHERE " + COLUMN_CARD_TYPE + " = \'" +
                type + "\' AND " + KEY_CUSTOMER + "=" + customer_id ;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        int elements = c.getCount();
        if(elements>0  ){
            Long mKey = 0L;
            String mType="";
            c.moveToFirst();
            if(!c.isNull(c.getColumnIndex(KEY_ID))){
                mKey = c.getLong(c.getColumnIndex(KEY_ID));
            }
            if(!c.isNull(c.getColumnIndex(COLUMN_CARD_ALIAS))){
                mType = c.getString(c.getColumnIndex(COLUMN_CARD_ALIAS));
                value.put(mKey,mType);
            }
        }
        c.close();
        return value;
    }


    /**
     *  Provides the list of credit card aliases for a given customer id
     * @param customer_ID
     * @return Credit card aliases list, returns null if no customer found
     */
    public String[] getCustomersCreditCardsList(long customer_ID){
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CREDIT_CARDS + " WHERE " + KEY_CUSTOMER + "=" +customer_ID;
        Log.e(LOG, selectQuery);
        String[] cardAliases=null;
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            c.moveToFirst();
            int numberOfCreditcards = c.getCount();
            if (numberOfCreditcards>0){
                if(!c.isNull(c.getColumnIndex(COLUMN_CARD_ALIAS))) {
                    cardAliases = new String[numberOfCreditcards];
                    int count = 0;
                    while (!c.isLast()) {
                        cardAliases[count] = c.getString(c.getColumnIndex(COLUMN_CARD_ALIAS));
                        count++;
                        c.moveToNext();
                    }
                    if (c.isLast()) {
                        cardAliases[count] = c.getString(c.getColumnIndex(COLUMN_CARD_ALIAS));
                    }
                }
            }
            c.close();
            return cardAliases;
        }else{
            return null;
        }
    }

    /**
     * Retrieves a HashMap with the credit Card Details
     * @param CardKey the primary key of the card
     * @return
     */
    public Map<String,String> getCreditCardDetails(long CardKey){
        Map<String, String> value = new HashMap<String, String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CREDIT_CARDS + " WHERE " + KEY_ID + "="+ CardKey;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            c.moveToFirst();
            if (c.getCount()>0){
                value.put(KEY_ID, (c.getLong(c.getColumnIndex(KEY_ID)))+"");
                value.put(COLUMN_CARD_CARDHOLDER_NAME, c.getString(c.getColumnIndex(COLUMN_CARD_CARDHOLDER_NAME)));
                value.put(COLUMN_CARD_CARDNUMBER, (c.getInt(c.getColumnIndex(COLUMN_CARD_CARDNUMBER)))+"");
                value.put(COLUMN_CARD_CVV, (c.getInt(c.getColumnIndex(COLUMN_CARD_CVV)))+"");
                value.put(COLUMN_CARD_ADDRESS, c.getString(c.getColumnIndex(COLUMN_CARD_ADDRESS)));
                value.put(COLUMN_CARD_CITY, c.getString(c.getColumnIndex(COLUMN_CARD_CITY)));
                value.put(COLUMN_CARD_ZIP,  (c.getInt(c.getColumnIndex(COLUMN_CARD_ZIP)))+"");
                value.put(COLUMN_CARD_ALIAS, c.getString(c.getColumnIndex(COLUMN_CARD_ALIAS)));
                value.put(COLUMN_CARD_TYPE, c.getString(c.getColumnIndex(COLUMN_CARD_TYPE)));

            }
            return value;
        }else{
            return null;
        }
    }

    /**
     * Retrieves the customer Information for a give Customer ID
     * @param CustomerID
     * @return
     */
    public Map<String,String> getCustomerDetails(long CustomerID){
        Map<String, String> value = new HashMap<String, String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMERS + " WHERE " + KEY_ID + "="+ CustomerID + ";";
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            c.moveToFirst();
            if (c.getCount()>0){
                value.put(KEY_ID, (c.getLong(c.getColumnIndex(KEY_ID)))+"");
                value.put(COLUMN_CUSTOMER_NAME, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_NAME)));
                value.put(COLUMN_CUSTOMER_SURNAME, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_SURNAME)));
                value.put(COLUMN_CUSTOMER_CITY, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_CITY)));
                value.put(COLUMN_CUSTOMER_STATE, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_STATE)));
                value.put(COLUMN_CUSTOMER_USERNAME, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_USERNAME)));
                value.put(COLUMN_CUSTOMER_PASSWORD,  c.getString(c.getColumnIndex(COLUMN_CUSTOMER_PASSWORD)));
                value.put(COLUMN_CUSTOMER_PREFERRED_TRANSPORT, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_PREFERRED_TRANSPORT)));
                value.put(COLUMN_CUSTOMER_ENTITY_ID, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_ENTITY_ID)));
                value.put(COLUMN_CUSTOMER_POLICY_TYPE, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_POLICY_TYPE)));
            }
            return value;
        }else{
            return null;
        }
    }

    public Map<String,String> getCustomerDetailsByEntity(int EntityID){
        Map<String, String> value = new HashMap<String, String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_CUSTOMERS + " WHERE " + COLUMN_CUSTOMER_ENTITY_ID + "="+ EntityID;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            c.moveToFirst();
            if (c.getCount()>0){
                value.put(KEY_ID, (c.getLong(c.getColumnIndex(KEY_ID)))+"");
                value.put(COLUMN_CUSTOMER_NAME, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_NAME)));
                value.put(COLUMN_CUSTOMER_SURNAME, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_SURNAME)));
                value.put(COLUMN_CUSTOMER_CITY, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_CITY)));
                value.put(COLUMN_CUSTOMER_STATE, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_STATE)));
                value.put(COLUMN_CUSTOMER_USERNAME, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_USERNAME)));
                value.put(COLUMN_CUSTOMER_PASSWORD,  c.getString(c.getColumnIndex(COLUMN_CUSTOMER_PASSWORD)));
                value.put(COLUMN_CUSTOMER_PREFERRED_TRANSPORT, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_PREFERRED_TRANSPORT)));
                value.put(COLUMN_CUSTOMER_ENTITY_ID, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_ENTITY_ID)));
                value.put(COLUMN_CUSTOMER_POLICY_TYPE, c.getString(c.getColumnIndex(COLUMN_CUSTOMER_POLICY_TYPE)));
            }
            return value;
        }else{
            return null;
        }
    }


    public Map<String,String> getBusDriverDetails(long busDriver_id){
        Map<String, String> value = new HashMap<String, String>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + TABLE_BUS_DRIVERS + " WHERE " + KEY_ID + "="+ busDriver_id;
        Log.e(LOG, selectQuery);
        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null){
            c.moveToFirst();
            if (c.getCount()>0){
                value.put(KEY_ID, String.valueOf((c.getLong(c.getColumnIndex(KEY_ID)))));
                value.put(COLUMN_BUS_DRIVER_NAME, c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_NAME)));
                value.put(COLUMN_BUS_DRIVER_SURNAME, c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_SURNAME)));
                value.put(COLUMN_BUS_DRIVER_CITY, c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_CITY)));
                value.put(COLUMN_BUS_DRIVER_ADDRESS, c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_ADDRESS)));
                value.put(COLUMN_BUS_DRIVER_USERNAME, c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_USERNAME)));
                value.put(COLUMN_BUS_DRIVER_PASSWORD,  c.getString(c.getColumnIndex(COLUMN_BUS_DRIVER_PASSWORD)));
                value.put(COLUMN_BUS_DRIVER_ZIP, String.valueOf(c.getInt(c.getColumnIndex(COLUMN_BUS_DRIVER_ZIP))));
            }
            return value;
        }else{
            return null;
        }
    }


    /**
     * Closes Database
     */
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }
}
