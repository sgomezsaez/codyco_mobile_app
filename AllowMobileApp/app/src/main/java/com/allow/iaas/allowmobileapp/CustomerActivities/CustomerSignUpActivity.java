package com.allow.iaas.allowmobileapp.CustomerActivities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Button;
;

//////////////// Adapted to integrate with Context 20 services (UoC) ///////////////////
//import com.Wsdl2Code.WebServices.context.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.context_v20.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.context_v20.VectoruserPreference;
import com.Wsdl2Code.WebServices.context_v20.userPreference;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
//import com.Wsdl2Code.WebServices.context.Passenger;
import com.Wsdl2Code.WebServices.context_v20.passengerDataType;
//import com.Wsdl2Code.WebServices.context.context;
import com.Wsdl2Code.WebServices.context_v20.context_v20;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.database.Preference;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class CustomerSignUpActivity extends Activity implements IWsdl2CodeEvents{

    private static final String TAG1 = "CUSTOMER" ;
    public static final int USER_CREATE = 1 ;
    public static final int USER_UPDATE = 2 ;

    public static final int EXISTENT_USER = 1 ;
    public static final int NEW_USER = 2 ;

    private static final String  TAG = "SignUp";

    private EditText eTname, eTsurname, eTCity, eTstate, eTusername, eTpassword, eTrepassword, eTravelTime, eTravelCost, eWalkingDistance, eMaxNoChanges, eTravelTimeWeight, eTravelCostWeight, eWalkingDistanceWeight, eMaxNoChangesWeight;
    private Button SignUpButton;
    private CheckBox checkBox_conditions;
    protected mySQLiteHelper db;

    private View mProgressView;
    private View mSignUpFormView;

    protected Spinner transports, sorting, paymentMethod, passengerType, decisionPolicy;
    protected Context ActivityContext;

    Map<String,CreditCard> tempCards = new HashMap<String,CreditCard>();
    int instruction;
    long idCustomer;
    String customerUUID = null;

    context_v20 context_webservice =  null;

    @Override
    protected void onResume() {
        super.onResume();
        db  = new mySQLiteHelper(getApplicationContext());
        populatePaymentMethods(idCustomer, instruction);
    }

    @Override
    protected void onPause() {
        super.onPause();
        db.closeDB();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custormer_sign_up);
        ActivityContext = this;
        Intent callerIntent = getIntent();
        instruction = callerIntent.getIntExtra("instruction", USER_CREATE);

        //We bind the UI elements
        SignUpButton = (Button) findViewById(R.id.button_SignUp);
        eTname = (EditText) findViewById(R.id.editTextName);
        eTsurname = (EditText) findViewById(R.id.editTextSurname);
        eTCity = (EditText) findViewById(R.id.editTextCity);
        eTstate = (EditText) findViewById(R.id.editTextState);
        eTusername = (EditText) findViewById(R.id.editTextUserName);
        eTpassword = (EditText) findViewById(R.id.editTextPass);
        eTrepassword = (EditText) findViewById(R.id.editTextRepPass);
        mSignUpFormView = findViewById(R.id.signUp_form_customer);
        mProgressView = findViewById(R.id.signUp_progress_customer);
        passengerType = (Spinner) findViewById(R.id.spinnerPassengerType);
        decisionPolicy = (Spinner) findViewById(R.id.spinnerDecisionPolicy);

        //Preferences
        eTravelTime = (EditText) findViewById(R.id.editTextMaxTravelTime);
        eTravelTimeWeight = (EditText) findViewById(R.id.editTextMaxTravelTimeWeight);
        eTravelCost = (EditText) findViewById(R.id.editTextMaxTravelCost);
        eTravelCostWeight = (EditText) findViewById(R.id.editTextMaxTravelCostWeight);
        eWalkingDistance = (EditText) findViewById(R.id.editTextMaxWalkingDistance);
        eWalkingDistanceWeight = (EditText) findViewById(R.id.editTextMaxWalkingDistanceWeight);
        eMaxNoChanges = (EditText) findViewById(R.id.editTextMaxNoChanges);
        eMaxNoChangesWeight = (EditText) findViewById(R.id.editTextMaxNoChangesWeight);
        transports = (Spinner) findViewById(R.id.spinner_Transports);
        sorting= (Spinner) findViewById(R.id.spinnerResultSorting);

        // Payment
        paymentMethod= (Spinner) findViewById(R.id.spinnerPaymentMethod);
        checkBox_conditions = (CheckBox) findViewById(R.id.checkBox_TermsAndConditions);


        //We open the internal database
        db = new mySQLiteHelper(getApplicationContext());
        //Create WebserviceClient
        context_webservice = new context_v20(this, MyConstants.ENDPOINT_CONTEXT, 10000);

        //Populate the spinner of the different Transport Systems
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.transportSystems_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        transports.setAdapter(adapter);


        ArrayAdapter<CharSequence> adapter2 = ArrayAdapter.createFromResource(this,
                R.array.SortingType_array, android.R.layout.simple_spinner_item);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sorting.setAdapter(adapter2);

        // Populate Spinner with Passenger Types
        ArrayAdapter<CharSequence> adapterPassengerType = ArrayAdapter.createFromResource(this,
                R.array.PassengerType_array, android.R.layout.simple_spinner_item);
        adapterPassengerType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        passengerType.setAdapter(adapterPassengerType);

        // Populate Spinner with Decision Policies
        ArrayAdapter<CharSequence> adapterDecisionPolicy = ArrayAdapter.createFromResource(this,
                R.array.DecisionPolicyType_array, android.R.layout.simple_spinner_item);
        adapterDecisionPolicy.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        decisionPolicy.setAdapter(adapterDecisionPolicy);


        //If it user update we need to retrieve the information to populate all the fields
        if (instruction == USER_UPDATE){
            SignUpButton.setText("Update");
            String mUsername = callerIntent.getStringExtra("username");
            String mPassword = callerIntent.getStringExtra("password");
            idCustomer = db.getPrimaryKeyForCustomer(mUsername,mPassword);
            checkBox_conditions.setChecked(true);
            Map<String,String> data = db.getCustomerDetails(idCustomer);
            if(data != null){
                eTname.setText(data.get(mySQLiteHelper.COLUMN_CUSTOMER_NAME));
                eTsurname.setText(data.get(mySQLiteHelper.COLUMN_CUSTOMER_SURNAME));
                eTCity.setText(data.get(mySQLiteHelper.COLUMN_CUSTOMER_CITY));
                eTstate.setText(data.get(mySQLiteHelper.COLUMN_CUSTOMER_STATE));
                eTusername.setText(data.get(mySQLiteHelper.COLUMN_CUSTOMER_USERNAME));
                eTpassword.setText(data.get(mySQLiteHelper.COLUMN_CUSTOMER_PASSWORD));
                eTrepassword.setText(data.get(mySQLiteHelper.COLUMN_CUSTOMER_PASSWORD));
                customerUUID = data.get(mySQLiteHelper.COLUMN_CUSTOMER_ENTITY_ID);


                List<Preference> userPreferences = db.getPreferencesUser(customerUUID);
                for (Iterator<Preference> i = userPreferences.iterator(); i.hasNext(); ) {
                    Preference preference = i.next();
                    if (preference.getPreferenceName().equals(MyConstants.TRAVEL_TIME)) {
                        eTravelTime.setText(Double.toString(preference.getPreferenceValue()));
                        eTravelTimeWeight.setText(Double.toString(preference.getPreferenceWeight()));
                    }
                    else if (preference.getPreferenceName().equals(MyConstants.COST)) {
                        eTravelCost.setText(Double.toString(preference.getPreferenceValue()));
                        eTravelCostWeight.setText(Double.toString(preference.getPreferenceWeight()));
                    }
                    else if (preference.getPreferenceName().equals(MyConstants.WALKING_DISTANCE)) {
                        eWalkingDistance.setText(Double.toString(preference.getPreferenceValue()));
                        eWalkingDistanceWeight.setText(Double.toString(preference.getPreferenceWeight()));
                    }
                    else if (preference.getPreferenceName().equals(MyConstants.NUMBER_OF_CHANGES)) {
                        eMaxNoChanges.setText(Double.toString(preference.getPreferenceValue()));
                        eMaxNoChangesWeight.setText(Double.toString(preference.getPreferenceWeight()));
                    }
                }
            }
        }else if (instruction == USER_CREATE){
            idCustomer =db.getNewPrimaryKey(mySQLiteHelper.TABLE_CUSTOMERS);
        }

    }

    protected long getCustomerID(){
        return idCustomer;
    }

    protected String getCustumerUUID() {
        return this.customerUUID;
    }


    public void populatePaymentMethods(long idCustomer, int instruction){
        //Preparate the list of the diferent stored Cards
        List<String> list = new ArrayList<String>();
        list.add("Payment Methods");
        String[] creditCards =null;
        //If is an update Operation retrieves the currently stored Cards else uses the tmpCardList
        if(instruction == CustomerSignUpActivity.USER_UPDATE){
            creditCards= db.getCustomersCreditCardsList(idCustomer);
        }else if(instruction == CustomerSignUpActivity.USER_CREATE){
            creditCards = new String[tempCards.size()];
            int i=0;
            for(CreditCard card : tempCards.values()){
                creditCards[i] = card.getAlias();
                i++;
            }
        }
        list.add("Add Payment method");
        if (creditCards != null){
            for(String card: creditCards){
                list.add(card);
            }
        }
        ArrayAdapter<String> CardListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        CardListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paymentMethod.setAdapter(CardListAdapter);
        paymentMethod.setOnItemSelectedListener(new ClickListenerPayments());
    }

    private class ClickListenerPayments implements AdapterView.OnItemSelectedListener{

        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            String selectedElement = (String) parent.getItemAtPosition(position);

            switch(selectedElement){
                case "Add Payment method":
                    if (instruction == USER_CREATE){
                        Intent mIntent = new Intent(ActivityContext, CardDetailsActivity.class);
                        mIntent.putExtra("operation", CardDetailsActivity.CARD_CREATE);
                        mIntent.putExtra("user-type",NEW_USER);
                        startActivityForResult(mIntent, 2);
                    }else if(instruction == USER_UPDATE){
                        Intent mIntent = new Intent(ActivityContext, CardDetailsActivity.class);
                        mIntent.putExtra("operation", CardDetailsActivity.CARD_CREATE);
                        mIntent.putExtra("user-type",EXISTENT_USER);
                        mIntent.putExtra("customer_id", getCustomerID());
                        startActivity(mIntent);
                    }
                    break;
                case "Payment Methods":
                    Log.d(TAG,"payment presssed");
                    break;
                default:
                    if (instruction == USER_CREATE){
                        Intent mIntent = new Intent(ActivityContext, CardDetailsActivity.class);
                        mIntent.putExtra("operation", CardDetailsActivity.CARD_EDIT_NEW);
                        mIntent.putExtra("old_alias", selectedElement);
                        mIntent.putExtra("user-type",NEW_USER);
                        CreditCard selectedCard = tempCards.get(selectedElement);
                        mIntent.putExtras(convertCardToBundle(selectedCard));
                        startActivityForResult(mIntent, 3);
                    }else if(instruction == USER_UPDATE){
                        Intent mIntent = new Intent(ActivityContext, CardDetailsActivity.class);
                        mIntent.putExtra("operation", CardDetailsActivity.CARD_UPDATE);
                        mIntent.putExtra("user-type",EXISTENT_USER);
                        mIntent.putExtra("old_alias", parent.getItemAtPosition(position).toString());
                        mIntent.putExtra("customer_id", getCustomerID());
                        startActivity(mIntent);
                    }
                    break;
            }
        }
        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);

        try{
            if(resultCode == CardDetailsActivity.RESULT_CODE_CREATE_CARD_OF_NEW_USER ||
                    resultCode == CardDetailsActivity.RESULT_CODE_EDIT_CARD_OF_NEW_USER){

                String Cardholder = data.getStringExtra(CardDetailsActivity.CARDHOLDER);
                int CardNumber =  data.getIntExtra(CardDetailsActivity.CARDNUMBER, 0);
                int CVV = data.getIntExtra(CardDetailsActivity.CVV,0);
                String Address = data.getStringExtra(CardDetailsActivity.ADDRESS);
                String City =  data.getStringExtra(CardDetailsActivity.CITY);
                int ZIP = data.getIntExtra(CardDetailsActivity.ZIP, 0);
                String alias = data.getStringExtra(CardDetailsActivity.ALIAS);
                String type = data.getStringExtra(CardDetailsActivity.TYPE);

                if(resultCode ==CardDetailsActivity.RESULT_CODE_CREATE_CARD_OF_NEW_USER){
                    tempCards.put(alias, new CreditCard(Cardholder, CardNumber,CVV, Address,City,ZIP,alias,type));
                }else if(resultCode ==CardDetailsActivity.RESULT_CODE_EDIT_CARD_OF_NEW_USER){
                    String old_alias = data.getStringExtra("old_alias");
                    tempCards.remove(old_alias);
                    tempCards.put(alias, new CreditCard(Cardholder, CardNumber, CVV, Address,City,ZIP,alias, type));
                }
            }else if(resultCode== CardDetailsActivity.RESULT_CODE_DELETE_CARD){
                String old_alias = data.getStringExtra("old_alias");
                tempCards.remove(old_alias);
            }
        }catch (Exception e){
            Log.e(TAG,"exception",e);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_custormer_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSignUpClick(View view){
        View focusView = null;
        String name = eTname.getText().toString();
        String surname = eTsurname.getText().toString();
        String city = eTCity.getText().toString();
        String state = eTstate.getText().toString();
        String username = eTusername.getText().toString();
        String password = eTpassword.getText().toString();
        String repassword = eTrepassword.getText().toString();
        String prefTransport = transports.getSelectedItem().toString();
        String passengerTypeString = passengerType.getSelectedItem().toString();
        String decisionPolicyString = decisionPolicy.getSelectedItem().toString();


        if (name.length()<1){
            eTname.setError("Give a Valid Name");
            eTname.setText("");
            eTname.requestFocus();
            return;
        }
        if (surname.length()<1){
            eTsurname.setError("Give a Valid Surname");
            eTsurname.setText("");
            eTsurname.requestFocus();
            return;
        }
        if (city.length()<1){
            eTCity.setError("Give a Valid City");
            eTCity.setText("");
            eTCity.requestFocus();
            return;
        }
        if (state.length()<1){
            eTstate.setError("Give a State");
            eTstate.setText("");
            eTstate.requestFocus();
            return;
        }
        if (username.length()<1){
            eTusername.setError("Give a Username");
            eTusername.setText("");
            eTusername.requestFocus();
            return;
        }
        if (password.length()<4){
            eTpassword.setError("Give a Password of at leas 5 Characters");
            eTpassword.setText("");
            eTpassword.requestFocus();
            return;
        }
        if (repassword.length()<4){
            eTrepassword.setError("Retype your Password");
            eTrepassword.setText("");
            eTrepassword.requestFocus();
            return;
        }
        if(!password.equals(repassword)){
            eTrepassword.setError("Password and repassword not equal");
            eTrepassword.setText("");
            eTrepassword.requestFocus();
            return;
        }

        if(!checkBox_conditions.isChecked()){
            checkBox_conditions.setError("You need to Accept the terms and conditions");
            checkBox_conditions.requestFocus();
            return;
        }
        String transportMean ="";
        switch (prefTransport){
            case "Bus":
                transportMean = "bus";
                break;
            case "Taxi":
                transportMean = "taxi";
                break;
            case "Car Pooling":
                transportMean = "car_pooling";
                break;
        }

        String passType = "";
        switch (passengerTypeString) {
            case "Worker":
                passType = MyConstants.PASSENGER_WORKER;
                break;
            case "Retired":
                passType = MyConstants.PASSENGER_RETIRED;
                break;
            case "Student":
                passType = MyConstants.PASSENGER_STUDENT;
                break;
        }

        String decPolicy = "";
        switch (decisionPolicyString) {
            case "utility":
                decPolicy = MyConstants.POLICY_UTILITY;
                break;
            case "price":
                decPolicy = MyConstants.POLICY_COST;
                break;
            case "duration":
                decPolicy = MyConstants.POLICY_DURATION;
                break;
            case "manual selection":
                decPolicy = MyConstants.POLICY_NONE;
                break;
        }

        if (instruction == USER_CREATE){
            if (customerUUID == null) {
                this.customerUUID = UUID.randomUUID().toString();
            }
            System.out.println("Created Customer UUID: " + customerUUID);
            this.idCustomer = db.createCustomer(customerUUID, name, surname, city, state, username, password, transportMean, decPolicy);
            System.out.println("Created Customer with ID " + idCustomer);
            System.out.println("Inserting Customer Preferences...");
            ArrayList<Preference> userPreferencesList = this.createUserPreferencesList();
            db.insertPreferencesUser(this.customerUUID, userPreferencesList);
            if(tempCards.size()>0){
                for(CreditCard card: tempCards.values()){
                    long newCardKey =db.getNewPrimaryKey(mySQLiteHelper.TABLE_CREDIT_CARDS);
                    db.createCreditCard(newCardKey,idCustomer,card.getCardholder(),card.getCardNumber(),card.getCVV(),card.getAddress(),card.getCity(),card.getZIP(),card.getAlias(),card.getType());
                }
            }

            if (MyConstants.IS_PRODUCTION){
                try {
                    System.out.println("Entering Contaxt Service...");
                    passengerDataType passenger = new passengerDataType();
                    passenger.passengerID = this.customerUUID;
                    passenger.name = name;
                    passenger.surname = surname;
                    passenger.city = city;
                    passenger.userName = username;
                    passenger.decisionPolicy = decPolicy;
                    if (passType != null) {
                        passenger.passengerType = passType;
                    }
                    VectoruserPreference userPreferences = this.initializeVectorPreferences();
                    passenger.preferencesList = userPreferences;
                    context_webservice.url = MyConstants.ENDPOINT_CONTEXT;
                    context_webservice.createPassengerAsync(passenger);
                } catch (Exception e) {
                    Log.e(TAG, "Error Service CreatePassenger");
                    Log.e(TAG, e.getLocalizedMessage());
                    Log.e(TAG, e.getStackTrace().toString());
                }
            }else{
                finish();
            }
        } else if (instruction ==USER_UPDATE){

            VectoruserPreference userPreferences = this.initializeVectorPreferences();
            db.updateCustomer(customerUUID, name, surname, city, state, username, password, transportMean, decPolicy);
            ArrayList<Preference> userPreferencesList = this.createUserPreferencesList();
            db.updatePreferenceUser(customerUUID, userPreferencesList);
            db.closeDB();
            if (MyConstants.IS_PRODUCTION){

                try {
                    context_webservice.updatePassengerAsync(name, surname, city, username, passType, userPreferences, decPolicy);
                } catch (Exception e) {
                    Log.e(TAG, "Error Updating Service CreatePassenger");
                }
            }else{
                finish();
            }
        }

    }

    private ArrayList<Preference> createUserPreferencesList () {
        ArrayList<Preference> preferencesList = new ArrayList<Preference>();
        preferencesList.add(new Preference(MyConstants.TRAVEL_TIME, MyConstants.TRAVEL_TIME,
                Double.parseDouble(eTravelTime.getText().toString()), Double.parseDouble(eTravelTimeWeight.getText().toString())));
        preferencesList.add(new Preference(MyConstants.WALKING_DISTANCE, MyConstants.WALKING_DISTANCE,
                Double.parseDouble(eWalkingDistance.getText().toString()), Double.parseDouble(eWalkingDistanceWeight.getText().toString())));
        preferencesList.add(new Preference(MyConstants.COST, MyConstants.COST,
                Double.parseDouble(eTravelCost.getText().toString()), Double.parseDouble(eTravelCostWeight.getText().toString())));
        preferencesList.add(new Preference(MyConstants.NUMBER_OF_CHANGES, MyConstants.NUMBER_OF_CHANGES,
                Double.parseDouble(eMaxNoChanges.getText().toString()), Double.parseDouble(eMaxNoChangesWeight.getText().toString())));
        return preferencesList;
    }

    private VectoruserPreference initializeVectorPreferences () {
        VectoruserPreference v = new VectoruserPreference();
        //     public userPreference(String preferenceID, String preferenceName, String preferenceValue, double preferenceWeight) {
        v.add(new userPreference(MyConstants.TRAVEL_TIME, MyConstants.TRAVEL_TIME,
                eTravelTime.getText().toString(), Double.parseDouble(eTravelTimeWeight.getText().toString())));
        v.add(new userPreference(MyConstants.WALKING_DISTANCE, MyConstants.WALKING_DISTANCE,
                eWalkingDistance.getText().toString(), Double.parseDouble(eWalkingDistanceWeight.getText().toString())));
        v.add(new userPreference(MyConstants.COST, MyConstants.COST,
                eTravelCost.getText().toString(), Double.parseDouble(eTravelCostWeight.getText().toString())));
        v.add(new userPreference(MyConstants.NUMBER_OF_CHANGES, MyConstants.NUMBER_OF_CHANGES,
                eMaxNoChanges.getText().toString(), Double.parseDouble(eMaxNoChangesWeight.getText().toString())));
        return v;
    }

    /**
     * Shows the progress UI and hides the SignUp form, during the ProgressBar Animation
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mSignUpFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mSignUpFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void Wsdl2CodeStartedRequest() {

        System.out.println("Request Started");
        showProgress(true);
    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object Data) {
        System.out.println("Method " + methodName + " Finished");
        switch (methodName){
            case "createPassenger":
                String result = (String) Data;
                System.out.println("Retrieved from Service: ");
                System.out.println(result);
                /*int EntityId = (int)Data;
                if(EntityId>0){
                    db.updateCustomerWithEntityID(idCustomer, customerUUID);
                }*/
                System.out.println("Finished CreatePassenger Operation");
                db.closeDB();
                break;
            case "updatePassenger":
                break;
        }
        finish();

    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {
        System.out.println("System Finished with Exception!!!!!!");
        System.out.println(ex.getMessage());
    }

    @Override
    public void Wsdl2CodeEndedRequest() {
        showProgress(true);
    }

    private Bundle convertCardToBundle(CreditCard card){
        Bundle mBundle = new Bundle();
        mBundle.putString("Cardholder", card.getCardholder());
        mBundle.putInt("CardNumber", card.getCardNumber());
        mBundle.putInt("CVV", card.getCVV());
        mBundle.putString("Address", card.getAddress());
        mBundle.putString("City", card.getCity());
        mBundle.putInt("ZIP", card.getZIP());
        mBundle.putString("alias", card.getAlias());
        mBundle.putString("type", card.getType());
        return mBundle;
    }


    private class CreditCard{

        private String Cardholder;
        private int CardNumber;
        private int CVV;
        private String Address;
        private String City;
        private int ZIP;
        private String alias;
        private String Type;

        public CreditCard( String Cardholder, int CardNumber,  int CVV,  String Address, String City,   int ZIP, String alias, String Type){
            this.Cardholder = Cardholder;
            this.CardNumber = CardNumber;
            this.CVV = CVV;
            this.Address =Address;
            this.City = City;
            this.ZIP = ZIP;
            this.alias =alias;
            this.Type = Type;
        }

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public String getCardholder() {
            return Cardholder;
        }

        public void setCardholder(String cardholder) {
            Cardholder = cardholder;
        }

        public int getCardNumber() {
            return CardNumber;
        }

        public void setCardNumber(int cardNumber) {
            CardNumber = cardNumber;
        }

        public int getCVV() {
            return CVV;
        }

        public void setCVV(int CVV) {
            this.CVV = CVV;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String address) {
            Address = address;
        }

        public String getCity() {
            return City;
        }

        public void setCity(String city) {
            City = city;
        }

        public int getZIP() {
            return ZIP;
        }

        public void setZIP(int ZIP) {
            this.ZIP = ZIP;
        }

        public String getType(){
            return Type;
        }

        public void setType(String Type){
            this.Type = Type;
        }

    }
}
