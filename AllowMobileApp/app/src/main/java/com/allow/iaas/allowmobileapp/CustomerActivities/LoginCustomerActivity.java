package com.allow.iaas.allowmobileapp.CustomerActivities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.Wsdl2Code.WebServices.context_v20.IWsdl2CodeEvents;
import com.Wsdl2Code.WebServices.context_v20.VectoruserPreference;
import com.allow.iaas.allowmobileapp.helpers.MyConstants;
import com.Wsdl2Code.WebServices.context_v20.passengerDataType;
import com.Wsdl2Code.WebServices.context.context;
import com.Wsdl2Code.WebServices.context_v20.context_v20;
import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.database.Preference;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * A login screen that offers login via username/password.
 */
public class LoginCustomerActivity extends Activity implements IWsdl2CodeEvents {

    private String[] credentialsList;
    String EntityID = null;
    String pref_modes;

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;
    private UserLoginToUpdateTask mUpdateTask =null;

    // UI references.
    private AutoCompleteTextView mUsernameView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mUsernameSignInButton;

    // Database Helper
    protected mySQLiteHelper db;
    private Intent mIntent;
    private Context activityContext;

    private String passenger = null;
    long passenger_id = -1;


    /**
     * Each time we get into this screen we need to update the data base and we need to  update the credentials list
     */
    @Override
    protected void onResume() {
        super.onResume();
        db =  new mySQLiteHelper(getApplicationContext());
        credentialsList = db.getUser_Pass_Keys();
        // if there are some credentials available populate in autocomplete
        if (credentialsList != null){
            populateAutoComplete();
        }
    }

    /**
     * Each time we get out from this screen we ned to  close de the database
     */
    @Override
    protected void onPause() {
        super.onPause();
        db.closeDB();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_login);
        //The database is loaded and the contained credentials are requested
        db = new mySQLiteHelper(getApplicationContext());
        credentialsList = db.getUser_Pass_Keys();

        // Retrieve UI elements
        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username_customer);
        mLoginFormView = findViewById(R.id.login_form_customer);
        mProgressView = findViewById(R.id.login_progress_customer);
        mUsernameSignInButton = (Button) findViewById(R.id.username_sign_in_button_customer);

        //Setup of the button to make the put the text Login in the keyboard
        mPasswordView = (EditText) findViewById(R.id.password_customer);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login_customer || id == EditorInfo.IME_NULL) {
                    //Do Login
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mUsernameSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        activityContext = this;
    }

    /**
     * Auto pupulates with the stored usernames
     */
    private void populateAutoComplete() {
        if (VERSION.SDK_INT >= 8) {
            // Use AccountManager (API 8+)
            new SetupUsernameAutocompleteTask().execute(null, null);
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made, IS_PRODUCTION == false
     * then the information wouldn't be validated with the context web service and it will
     * be just be checked against the internal database
     */
    public void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            //If the username and the password are stored in the Internal Database then..
            if (internalLogin(username,password)) {

                if (MyConstants.IS_PRODUCTION){
                    // Show a progress spinner, and kick off a background task to
                    // perform the user login attempt.
                    long customerPK = db.getPrimaryKeyForCustomer(username, password);
                    Map<String, String> customerInfo = db.getCustomerDetails(customerPK);
                    EntityID = customerInfo.get(mySQLiteHelper.COLUMN_CUSTOMER_ENTITY_ID);
                    pref_modes = customerInfo.get(mySQLiteHelper.COLUMN_CUSTOMER_PREFERRED_TRANSPORT);

                    //context srv1 = new context(this);
                    context_v20 srv1 = new context_v20(this);
                    srv1.setUrl(MyConstants.ENDPOINT_CONTEXT);
                    try {
                        srv1.retrievePassengerAsync(EntityID);
                        System.out.print(EntityID);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else{
                    showProgress(true);
                    mAuthTask = new UserLoginTask(username, password);
                    mAuthTask.execute((Void) null);
                }

            }else{
                mPasswordView.setError(getString(R.string.error_incorrect_login));
                mPasswordView.requestFocus();
            }

        }
    }

    /**
     * Checks if the username is longer than 1 symbol
     * @param username to be tested
     * @return true if the name is valid, otherwise
     */
    private boolean isUsernameValid(String username) {
        return username.length()>0;
    }

    /**
     * Check if the password is larger than 4 characters
     * @param password to be checked
     * @return is true if the password is longer than 4 characters.
     */
    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }



    /**
     * Shows the progress UI and hides the login form, during the ProgressBar Animation
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    public void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            // if (false) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }


    /**
     * It is called when Button Register is pressed
     * @param view passed to this button
     */
    public void clickOnRegister(View view){
        Intent mSingUpIntent = new Intent(this, CustomerSignUpActivity.class);
        mSingUpIntent.putExtra("instruction", CustomerSignUpActivity.USER_CREATE);
        startActivity(mSingUpIntent);
    }

    /**
     *  It is called when Button EditData is pressed
     * @param view passed to this button
     */
    public void clickOnEditData(View view){

        if(mUpdateTask != null){
            return;
        }
        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;


        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid username.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            mUsernameView.setError(getString(R.string.error_invalid_username));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mUpdateTask = new UserLoginToUpdateTask(username, password);
            mUpdateTask.execute((Void) null);
        }

    }


    private boolean internalLogin(String Username, String Password){

        if (credentialsList != null) {
            for (String credential : credentialsList) {
                String[] pieces = credential.split(":");
                if (pieces[0].equals(Username)) {
                    // Account exists, return true if the password matches.
                    return pieces[1].equals(Password);
                }
            }
        }
        return false;
    }

    @Override
    public void Wsdl2CodeStartedRequest() {
        //  onPreExecute
        showProgress(true);
    }

    @Override
    public void Wsdl2CodeFinished(String methodName, Object Data) {
        //  onPostExecute
        System.out.println("inished SOAP Call: processing response");
        switch (methodName){
            case "retrievePassenger":
               System.out.println("Retrieve Passenger Data: processing response");
                String passengerID = (String)Data;
                //Map<String,String> deails =db.getCustomerDetailsByEntity(mPassenger.id);
                long passenger_id = db.findUserTableID(this.EntityID);
                ArrayList<Preference> userPreferences = db.getPreferencesUser(this.EntityID);
                mIntent = new Intent(activityContext, MainCustomerSearchActivity.class);
                mIntent.putExtra("passenger_id", passenger_id);
                mIntent.putExtra("pref_modes", pref_modes);
                mIntent.putExtra("user_preferences", userPreferences);
                Map<String,String> passengerDetails = db.getCustomerDetails(passenger_id);
                mIntent.putExtra("policyType", passengerDetails.get(mySQLiteHelper.COLUMN_CUSTOMER_POLICY_TYPE));
                mIntent.putExtra("passengerUUID", this.EntityID);
                mIntent.putExtra("passengerName", passengerDetails.get(mySQLiteHelper.COLUMN_CUSTOMER_NAME) + " " + passengerDetails.get(mySQLiteHelper.COLUMN_CUSTOMER_SURNAME));
                startActivity(mIntent);
                break;
        }

    }

    @Override
    public void Wsdl2CodeFinishedWithException(Exception ex) {


    }

    @Override
    public void Wsdl2CodeEndedRequest() {
        //  onPostExecute before Wsdl2CodeFinished
        System.out.println("Request Finished: Preparing to Process");
        showProgress(false);
        Toast t = Toast.makeText(getApplicationContext(),"Login Successfull",Toast.LENGTH_SHORT);
        t.show();
    }


    /**
     * Use an AsyncTask to fetch the user's username on a background thread, and update
     * the username text field with results on the main UI thread.
     */
    class SetupUsernameAutocompleteTask extends AsyncTask<Void, Void, List<String>> {

        @Override
        protected List<String> doInBackground(Void... voids) {
            ArrayList<String> usernameCollection = new ArrayList<>();
            String[] Usernames = db.getCustomerUsernames();

            if (Usernames != null){
                Collections.addAll(usernameCollection, Usernames);
            }
            return usernameCollection;
        }
        @Override
        protected void onPostExecute(List<String> usernameCollection) {
            addUsernamesToAutoComplete(usernameCollection);
        }
    }

    private void addUsernamesToAutoComplete(List<String> usernameCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(LoginCustomerActivity.this,
                android.R.layout.simple_dropdown_item_1line, usernameCollection);
        mUsernameView.setAdapter(adapter);
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
        private final String mUsername;
        private final String mPassword;

        UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                return false;
            }

            if (credentialsList != null) {
                for (String credential : credentialsList) {
                    String[] pieces = credential.split(":");
                    if (pieces[0].equals(mUsername)) {
                        // Account exists, return true if the password matches.
                        return pieces[1].equals(mPassword);
                    }
                }
            }
            //if no match then
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                long mId = db.getPrimaryKeyForCustomer(mUsername, mPassword);
                Map<String,String> details =db.getCustomerDetails((int) mId);
                passenger_id = Long.parseLong(details.get(mySQLiteHelper.KEY_ID));
                pref_modes = details.get(mySQLiteHelper.COLUMN_CUSTOMER_PREFERRED_TRANSPORT);

                mIntent = new Intent(activityContext, MainCustomerSearchActivity.class);
                mIntent.putExtra("passenger_id", passenger_id);
                mIntent.putExtra("pref_modes", pref_modes);
                startActivity(mIntent);
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginToUpdateTask extends AsyncTask<Void, Void, Boolean> {
        private final String mUsername;
        private final String mPassword;

        UserLoginToUpdateTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {
                // Simulate network access.
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                return false;
            }

            if (credentialsList != null) {
                for (String credential : credentialsList) {
                    String[] pieces = credential.split(":");
                    if (pieces[0].equals(mUsername)) {
                        // Account exists, return true if the password matches.
                        return pieces[1].equals(mPassword);
                    }
                }
            }
            //if no match then
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mUpdateTask = null;
            showProgress(false);

            if (success) {
                mIntent = new Intent(activityContext, CustomerSignUpActivity.class);
                mIntent.putExtra("instruction", CustomerSignUpActivity.USER_UPDATE);
                mIntent.putExtra("username",mUsername);
                mIntent.putExtra("password",mPassword);
                startActivity(mIntent);
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_login));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mUpdateTask = null;
            showProgress(false);
        }
    }
}



