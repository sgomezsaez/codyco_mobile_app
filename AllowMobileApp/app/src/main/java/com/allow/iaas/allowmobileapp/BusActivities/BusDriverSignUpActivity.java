package com.allow.iaas.allowmobileapp.BusActivities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.allow.iaas.allowmobileapp.R;
import com.allow.iaas.allowmobileapp.helpers.database.mySQLiteHelper;

import java.util.Map;
import java.util.UUID;


public class BusDriverSignUpActivity extends Activity {

    private static final String TAG = "BUS_DRIVER" ;
    public static final int BUS_DRIVER_CREATE = 1 ;
    public static final int BUS_DRIVER_UPDATE = 2 ;

    public static final String INSTRUCTION_KEY = "instruction";
    public static final String USERNAME_KEY = "username";
    public static final String PASSWORD_KEY = "password";


    private String usernameToUpdate,passwordToUpdate;
    private int instruction;

    private EditText eTname, eTsurname, eTCity, eTzip, eTusername, eTpassword, eTrepassword, eTAddress;
    private Button signUpButton;
    protected mySQLiteHelper db;
    protected Context ActivityContext;
    private long BusDriver_id;
    private String busDriverUUID = null;

    @Override
    protected void onResume() {
        super.onResume();
        db  = new mySQLiteHelper(getApplicationContext());
    }

    @Override
    protected void onPause() {
        db.closeDB();
        super.onPause();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_driver_sign_up);
        ActivityContext = this;
        eTname = (EditText) findViewById(R.id.editTextName_BusDriver);
        eTsurname = (EditText) findViewById(R.id.editTextSurname_BusDriver);
        eTAddress= (EditText) findViewById(R.id.editTextStreetandNr_BusDriver);
        eTCity = (EditText) findViewById(R.id.editTextCity_BusDriver);
        eTzip = (EditText) findViewById(R.id.editTextZip_BusDriver);
        eTusername = (EditText) findViewById(R.id.editTextUserName_BusDriver);
        eTpassword = (EditText) findViewById(R.id.editTextPass_BusDriver);
        eTrepassword = (EditText) findViewById(R.id.editTextRepPass_BusDriver);
        signUpButton = (Button) findViewById(R.id.buttonSignUpBus);

        db = new mySQLiteHelper(getApplicationContext());


        Intent callerIntent = getIntent();
        instruction = callerIntent.getIntExtra(INSTRUCTION_KEY,1);

        if (instruction == BUS_DRIVER_UPDATE){
            signUpButton.setText("Update");
            String mUsername = callerIntent.getStringExtra("username");
            String mPassword = callerIntent.getStringExtra("password");
            BusDriver_id = db.getPrimaryKeyForBusDriver(mUsername, mPassword);
            Map<String,String> data = db.getBusDriverDetails(BusDriver_id);
            if (data!=null){
                eTname.setText(data.get(mySQLiteHelper.COLUMN_BUS_DRIVER_NAME));
                eTsurname.setText(data.get(mySQLiteHelper.COLUMN_BUS_DRIVER_SURNAME));
                eTAddress.setText(data.get(mySQLiteHelper.COLUMN_BUS_DRIVER_ADDRESS));
                eTCity.setText(data.get(mySQLiteHelper.COLUMN_BUS_DRIVER_CITY));
                eTzip.setText(data.get(mySQLiteHelper.COLUMN_BUS_DRIVER_ZIP));
                eTusername.setText(data.get(mySQLiteHelper.COLUMN_BUS_DRIVER_USERNAME));
                eTpassword.setText(data.get(mySQLiteHelper.COLUMN_BUS_DRIVER_PASSWORD));
                eTrepassword.setText(data.get(mySQLiteHelper.COLUMN_BUS_DRIVER_PASSWORD));
            }

        }else if(instruction == BUS_DRIVER_CREATE){
            signUpButton.setText("Sign Up");
            BusDriver_id =db.getNewPrimaryKey(mySQLiteHelper.TABLE_BUS_DRIVERS);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_bus_driver_sign_up, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSignUp_BusDriverClick(View view){



        String name =  eTname.getText().toString();
        String surname =  eTsurname.getText().toString();
        String address = eTAddress.getText().toString();
        String city = eTCity.getText().toString();
        String zip = eTzip.getText().toString();
        String username = eTusername.getText().toString();
        String password = eTpassword.getText().toString();
        String repassword = eTrepassword.getText().toString();
        int zipNr=0;
        if (name.length()<1){
            eTname.setError("Give a Valid Name");
            eTname.setText("");
            eTname.requestFocus();
            return;
        }
        if (surname.length()<1){
            eTsurname.setError("Give a Valid Surname");
            eTsurname.setText("");
            eTsurname.requestFocus();
            return;
        }
        if (address.length()<1){
            eTAddress.setError("Give an Adress");
            eTAddress.setText("");
            eTAddress.requestFocus();
            return;
        }
        if (city.length()<1){
            eTCity.setError("Give a Valid City");
            eTCity.setText("");
            eTCity.requestFocus();
            return;
        }
        if (zip.length()<1){
            eTzip.setError("Give an ZIP code");
            eTzip.setText("");
            eTzip.requestFocus();
            return;
        }else{
            zipNr=Integer.parseInt(zip);
        }

        if (username.length()<1){
            eTusername.setError("Give a Username");
            eTusername.setText("");
            eTusername.requestFocus();
            return;
        }
        if (password.length()<4){
            eTpassword.setError("Give a Password of at leas 5 Characters");
            eTpassword.setText("");
            eTpassword.requestFocus();
            return;
        }
        if (repassword.length()<4){
            eTrepassword.setError("Retype your Password");
            eTrepassword.setText("");
            eTrepassword.requestFocus();
            return;
        }
        if(!password.equals(repassword)){
            eTrepassword.setError("Password and repassword not equal");
            eTrepassword.setText("");
            eTrepassword.requestFocus();
            return;
        }


        if(instruction == BUS_DRIVER_CREATE){
            this.busDriverUUID = UUID.randomUUID().toString();
            db.createBusDriver(name, surname, address, city, zipNr, username, password, busDriverUUID);
            db.closeDB();
            finish();
        }else if (instruction == BUS_DRIVER_UPDATE){
            this.BusDriver_id = db.getPrimaryKeyForBusDriver(username, password);
            //    public int updateBusDriver(long id, String name, String surname, String address, String state,int zip, String username, String Password, String entityID){
            db.updateBusDriver(BusDriver_id, name, surname, address, city, zipNr, username, password, busDriverUUID);
            db.close();
            finish();
        }


    }
}
